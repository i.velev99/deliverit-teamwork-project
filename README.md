# DeliverIT TeamWork project

https://trello.com/b/taWBsVDM/teamwork2 Project made by Ivan Velev and Dimitar Iliev.

http://localhost:8080/swagger-ui.html

## **DeliverIT**

DeliverIt is a web application that serves the needs of a freight forwarding company.
DeliverIT's customers can place orders on international shopping sites
and have their parcels delivered to the company's warehouses 

## 
  - Visible without authentication
    - the application start page
    - the user login form
    - the user registration form

### Technologies
- *IntelliJ IDEA* - as a IDE during the whole development process
- *Spring MVC* - as an application framework
- *Thymeleaf* - as a template engine in order to load a big amount of data (beers, users). Also thymeleaf allows inserting and replacing views and imports, which makes the code way more readable and replaceable
- *MariaDB* - as database
- *Hibernate* - access the database
- *Spring Security* - for managing users, roles and authentication
- *Git* - for source control management and documentation / manual of the application
