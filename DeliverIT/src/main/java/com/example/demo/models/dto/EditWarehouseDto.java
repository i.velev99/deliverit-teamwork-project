package com.example.demo.models.dto;

import javax.validation.Valid;

public class EditWarehouseDto {
    private int id;
    private int addressId;
    @Valid
    private AddressDto address;

    public EditWarehouseDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }
}
