package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "shipments")
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shipment_id")
    private int id;

    @Column(name = "departure_date")
    private Date departure;

    @Column(name = "arrival_date")
    private Date arrival;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @JsonIgnoreProperties(value = {"customer", "warehouse","shipment"})
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "shipment_id")
    private Set<Parcel> parcels;

    public Shipment(int id, Date departure, Date arrival, Status status) {
        this.id = id;
        this.departure = departure;
        this.arrival = arrival;
        this.status = status;
        parcels = new HashSet<>();
    }

    public Shipment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<Parcel> getParcels() {
        return parcels;
    }

    public void setParcels(Set<Parcel> parcels) {
        this.parcels = parcels;
    }

}
