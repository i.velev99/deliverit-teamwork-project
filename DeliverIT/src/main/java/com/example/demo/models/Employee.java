package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class Employee extends User{

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "parcel_id")
    private Set<Parcel> createdParcels;

    @JoinColumn(name = "id")
    @JsonIgnore
    @OneToOne
    private Employee creator;

    public Employee() {
    }

    public Employee(int id, String firstName, String lastName, String email, String username, String password, Role role) {
        super(id, firstName, lastName, email, username, password, role);
        createdParcels = new HashSet<>();
        creator = this;
    }

    public Employee getCreator() {
        return creator;
    }

    public void setCreator(Employee creator) {
        this.creator = creator;
    }

    public Set<Parcel> getCreatedParcels() {
        return createdParcels;
    }

    public void setCreatedParcels(Set<Parcel> createdParcels) {
        this.createdParcels = createdParcels;
    }

}
