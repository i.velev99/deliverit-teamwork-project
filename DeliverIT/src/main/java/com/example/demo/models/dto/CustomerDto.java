package com.example.demo.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CustomerDto extends UserDto {

    @NotNull(message = "Address should have street name and cityId.")
    private AddressDto address;

    public CustomerDto() {
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }


}
