package com.example.demo.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull(message = "First name should not be null")
    @Size(min = 2, max = 100, message = "First name should be between 2 and 100")
    private String firstName;

    @NotNull(message = "Last name should not be null")
    @Size(min = 2, max = 100, message = "Last name should be between 2 and 100")
    private String lastName;

    @NotNull
    @Email(message = "The given email is not valid")
    private String email;

    @NotNull(message = "Username should not be null")
    @Size(min = 2, max = 50, message = "Username should be between 2 and 50")
    private String username;

    @NotNull(message = "Password should not be null")
    @Size(min = 2, max = 50, message = "Password should be between 2 and 50")
    private String password;

    @JsonIgnore
    @NotNull
    private int roleId;

    public UserDto() {
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
