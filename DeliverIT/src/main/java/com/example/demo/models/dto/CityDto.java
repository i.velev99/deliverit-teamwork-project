package com.example.demo.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CityDto {

    @NotNull(message = "Name cannot be empty")
    @Size(min = 2, max = 100, message = "Name must be between 2 and 100 symbols")
    private String name;

    @Size(max = 10)
    private int countryId;

    public CityDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}
