package com.example.demo.models.dto;

import javax.validation.constraints.Positive;


public class ParcelDto {

    @Positive(message = "weight should be positive")
    private double weight;

    @Positive(message = "categoryId should be positive")
    private int categoryId;

    @Positive(message = "customerId should be positive")
    private int customerId;

    @Positive(message = "warehouseId should be positive")
    private int warehouseId;

    @Positive(message = "shipmentId should be positive")
    private int shipmentId;

    public ParcelDto() {
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(int shipmentId) {
        this.shipmentId = shipmentId;
    }
}
