package com.example.demo.models.dto;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class RegisterCustomerDto {

    @NotEmpty(message = "Username can't be empty")
    @Size(min = 4 , max = 24, message = "Username must be between 4 and 24 symbols.")
    private String username;

    @NotEmpty(message = "Password can't be empty")
    @Size(min = 4 , max = 24, message = "Password must be between 4 and 24 symbols.")
    private String password;

    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 4 , max = 24, message = "Password must be between 4 and 24 symbols.")
    private String repeatPassword;

    @NotEmpty(message = "First name can't be empty")
    @Size(min = 2, max = 100,message = "First Name must be between 2 and 100 symbols.")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    @Size(min = 2, max = 100,message = "Last Name must be between 2 and 100 symbols.")
    private String lastName;

    @NotEmpty(message = "Email can't be empty")
    @Email
    private String email;

    @Valid
    private AddressDto address;

    public RegisterCustomerDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }
}
