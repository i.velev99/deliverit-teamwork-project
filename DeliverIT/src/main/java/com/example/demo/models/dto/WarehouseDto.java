package com.example.demo.models.dto;

import javax.validation.constraints.NotNull;


public class WarehouseDto {

    @NotNull(message = "Address should have street name and cityId.")
    private AddressDto address;

    public WarehouseDto() {
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }
}
