package com.example.demo.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class AddressDto {

    @NotNull(message = "Name cannot be empty")
    @Size(min = 5, max = 100, message = "Address must be between 5 and 100 symbols")
    private String address;

    private int cityId;

    public AddressDto() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
