package com.example.demo.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CountryDto {

    @NotNull(message = "Name cannot be empty")
    @Size(min = 2, max = 100, message = "Name must be between 2 and 100 symbols")
    private String name;

    public CountryDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
