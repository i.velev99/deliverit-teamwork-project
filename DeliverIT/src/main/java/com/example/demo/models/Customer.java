package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class Customer extends User {

    @JsonIgnoreProperties(value = "address")
    @OneToOne
    @JoinTable(
            name = "users_addresses",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "address_id")

    )
    private Address address;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "id")
    private Customer creator;

    public Customer() {
    }

    public Customer(int id, String firstName, String lastName, String email, String username, String password, Role role, Address address) {
        super(id, firstName, lastName, email, username, password, role);
        this.address = address;
        creator = this;
    }

    public Customer getCreator() {
        return creator;
    }

    public void setCreator(Customer creator) {
        this.creator = creator;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
