package com.example.demo.models.dto;

import org.springframework.format.annotation.DateTimeFormat;


import javax.validation.constraints.*;
import java.util.Date;

public class ShipmentDto {

    @NotNull(message = "Departure date cannot be null")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date departure;

    @NotNull(message = "Arrival date cannot be null")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date arrival;

    @NotNull(message = "StatusId cannot be null")
    @Min(1)
    @Max(3)
    private int statusId;

    public ShipmentDto() {
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
