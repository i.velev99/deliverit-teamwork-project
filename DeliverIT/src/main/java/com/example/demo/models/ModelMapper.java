package com.example.demo.models;

import com.example.demo.models.dto.*;
import com.example.demo.repositories.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {

    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;
    private final CustomerRepository customerRepository;
    private final ParcelRepository parcelRepository;
    private final ShipmentRepository shipmentRepository;
    private final WarehouseRepository warehouseRepository;
    private final AddressRepository addressRepository;
    private final EmployeeRepository employeeRepository;
    private final CategoryRepository categoryRepository;
    private final StatusRepository statusRepository;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    @Autowired
    public ModelMapper(CityRepository cityRepository, CountryRepository countryRepository,
                       CustomerRepository customerRepository, ParcelRepository parcelRepository,
                       ShipmentRepository shipmentRepository, WarehouseRepository warehouseRepository,
                       AddressRepository addressRepository, EmployeeRepository employeeRepository,
                       CategoryRepository categoryRepository, StatusRepository statusRepository,
                       RoleRepository roleRepository, UserRepository userRepository) {
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
        this.customerRepository = customerRepository;
        this.parcelRepository = parcelRepository;
        this.shipmentRepository = shipmentRepository;
        this.warehouseRepository = warehouseRepository;
        this.addressRepository = addressRepository;
        this.employeeRepository = employeeRepository;
        this.categoryRepository = categoryRepository;
        this.statusRepository = statusRepository;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    public Country fromDto(CountryDto countryDto) {
        Country country = new Country();
        dtoToObject(countryDto, country);
        return country;
    }

    public Country fromDto(CountryDto countryDto, int id) {
        Country country = countryRepository.getById(id);
        dtoToObject(countryDto, country);
        return country;
    }

    private void dtoToObject(CountryDto countryDto, Country country) {
        country.setName(countryDto.getName());
    }

    public City fromDto(CityDto cityDto) {
        City city = new City();
        dtoToObject(cityDto, city);
        return city;
    }

    public City fromDto(CityDto cityDto, int id) {
        City city = cityRepository.getById(id);
        dtoToObject(cityDto, city);
        return city;
    }

    public void dtoToObject(CityDto cityDto, City city) {
        city.setName(cityDto.getName());
    }

    public Address fromDto(AddressDto addressDto, int id) {
        Address address = addressRepository.getById(id);
        dtoToObject(addressDto, address);
        return address;
    }

    public Address fromDto(AddressDto addressDto) {
        Address address = new Address();
        dtoToObject(addressDto, address);
        return address;
    }

    public void dtoToObject(AddressDto addressDto, Address address) {
        address.setAddress(addressDto.getAddress());
        address.setCity(cityRepository.getById(addressDto.getCityId()));
    }

    public Warehouse fromDto(WarehouseDto dto) {
        Warehouse warehouse = new Warehouse();
        warehouse.setAddress(fromDto(dto.getAddress()));
        return warehouse;
    }

    public Customer fromDto(CustomerDto customerDto) {
        Customer customer = new Customer();
        dtoToObject(customerDto, customer);
        return customer;
    }



    public Customer fromDto(RegisterCustomerDto customerDto) {
        Customer customer = new Customer();
        dtoToObject(customerDto, customer);
        return customer;
    }

    public void dtoToObject(RegisterCustomerDto customerDto, Customer customer) {
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setEmail(customerDto.getEmail());
        customer.setUsername(customerDto.getUsername());
        customer.setPassword(customerDto.getPassword());
        customer.setRole(roleRepository.getById(2));
        customer.setCreator(customer);
    }

    public void dtoToObject(CustomerDto customerDto, Customer customer) {
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setEmail(customerDto.getEmail());
        customer.setUsername(customerDto.getUsername());
        customer.setPassword(customerDto.getPassword());
        customer.setRole(roleRepository.getById(2));
        customer.setCreator(customer);
    }

    public Customer fromDto(CustomerDto customerDto, int id) {
        Customer customer = customerRepository.getById(id);
        dtoToObject(customerDto, customer);
        return customer;
    }

    public void dtoToObject(EmployeeDto employeeDto, Employee employee) {
        employee.setFirstName(employeeDto.getFirstName());
        employee.setLastName(employeeDto.getLastName());
        employee.setEmail(employeeDto.getEmail());
        employee.setUsername(employeeDto.getUsername());
        employee.setPassword(employeeDto.getPassword());
        employee.setRole(roleRepository.getById(1));
        employee.setCreator(employee);
    }

    public Employee fromDto(EmployeeDto employeeDto, int id) {
        Employee employee = employeeRepository.getById(id);
        dtoToObject(employeeDto, employee);
        return employee;
    }

    public void dtoToObject(ParcelDto parcelDto, Parcel parcel) {
        parcel.setWeight(parcelDto.getWeight());
        parcel.setCategory(categoryRepository.getById(parcelDto.getCategoryId()));
        parcel.setCustomer(customerRepository.getById(parcelDto.getCustomerId()));
        parcel.setShipment(shipmentRepository.getById(parcelDto.getShipmentId()));
        parcel.setWarehouse(warehouseRepository.getById(parcelDto.getWarehouseId()));
    }

    public Parcel fromDto(ParcelDto parcelDto, int id) {
        Parcel parcel = parcelRepository.getById(id);
        dtoToObject(parcelDto, parcel);
        return parcel;
    }

    public void dtoToObject(ShipmentDto shipmentDto, Shipment shipment) {
        shipment.setArrival(shipmentDto.getArrival());
        shipment.setDeparture(shipmentDto.getDeparture());
        shipment.setStatus(statusRepository.getById(shipmentDto.getStatusId()));
    }

    public Shipment fromDto(ShipmentDto shipmentDto, int id) {
        Shipment shipment = shipmentRepository.getById(id);
        dtoToObject(shipmentDto, shipment);
        return shipment;
    }

    public EditUserDto fromUser(User user) {
        EditUserDto userDto = new EditUserDto();
        dtoToObject(user,userDto);
        return userDto;
    }

    public void dtoToObject(User user, EditUserDto dto) {
        dto.setId(user.getId());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setUsername(user.getUsername());
        dto.setPassword(user.getPassword());
        dto.setEmail(user.getEmail());
        dto.setRoleId(user.getRole().getId());
    }

    public Employee fromDtoEmployee(EditUserDto dto, int id) {
        Employee employee = employeeRepository.getById(id);
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setPassword(dto.getPassword());
        return employee;
    }

    public Customer fromDtoCustomer(EditUserDto dto, int id) {
        Customer customer = customerRepository.getById(id);
        customer.setFirstName(dto.getFirstName());
        customer.setLastName(dto.getLastName());
        customer.setPassword(dto.getPassword());
        customer.setAddress(addressRepository.getById(dto.getAddressId()));
        return customer;
    }

    public Address fromDto(EditUserDto dto, int addressId) {
        Address address = addressRepository.getById(addressId);
        address.setAddress(dto.getAddress().getAddress());
        address.setCity(cityRepository.getById(dto.getAddress().getCityId()));
        return address;
    }

    public Address fromDto(EditWarehouseDto dto, int addressId) {
        Address address = addressRepository.getById(addressId);
        address.setAddress(dto.getAddress().getAddress());
        address.setCity(cityRepository.getById(dto.getAddress().getCityId()));
        return address;
    }

    public EditWarehouseDto fromWarehouse(Warehouse warehouse) {
        EditWarehouseDto dto = new EditWarehouseDto();
        dtoToObject(warehouse, dto);
        return dto;
    }

    private void dtoToObject(Warehouse warehouse, EditWarehouseDto dto) {
        dto.setId(warehouse.getId());
        dto.setAddressId(warehouse.getAddress().getId());
    }

    public EditParcelDto fromParcel(Parcel parcel) {
        EditParcelDto dto = new EditParcelDto();
        dtoToObject(parcel, dto);
        return dto;
    }

    private void dtoToObject(Parcel parcel, EditParcelDto dto) {
        dto.setId(parcel.getId());
        dto.setCategoryId(parcel.getCategory().getId());
        dto.setWeight(parcel.getWeight());
        dto.setShipmentId(parcel.getShipment().getId());
        dto.setCustomerId(parcel.getCustomer().getId());
        dto.setWarehouseId(parcel.getWarehouse().getId());
    }

    public Warehouse fromDtoWarehouse(EditWarehouseDto dto, int id) {
        Warehouse warehouse = warehouseRepository.getById(id);
        warehouse.setAddress(addressRepository.getById(dto.getAddressId()));
        return warehouse;
    }

    public Parcel fromDtoParcel(EditParcelDto dto, int id) {
        Parcel parcel = parcelRepository.getById(id);
        parcel.setWeight(dto.getWeight());
        parcel.setCustomer(customerRepository.getById(dto.getCustomerId()));
        parcel.setShipment(shipmentRepository.getById(dto.getShipmentId()));
        parcel.setCategory(categoryRepository.getById(dto.getCategoryId()));
        parcel.setWarehouse(warehouseRepository.getById(dto.getWarehouseId()));
        return parcel;
    }

    public Parcel fromDto(ParcelDto dto) {
        Parcel parcel = new Parcel();
        dtoToObject(dto, parcel);
        return parcel;
    }

    public Shipment fromDto(ShipmentDto dto) {
        Shipment shipment = new Shipment();
        dtoToObject(dto, shipment);
        return shipment;

    }

    public EditShipmentDto fromShipment(Shipment shipment) {
        EditShipmentDto dto = new EditShipmentDto();
        dtoToObject(shipment, dto);
        return dto;
    }

    private void dtoToObject(Shipment shipment, EditShipmentDto dto) {
        dto.setId(shipment.getId());
        dto.setStatusId(shipment.getStatus().getId());
        dto.setArrival(shipment.getArrival());
        dto.setDeparture(shipment.getDeparture());
    }

    public Shipment fromDtoShipment(EditShipmentDto dto, int id) {
        Shipment shipment = shipmentRepository.getById(id);
        shipment.setStatus(statusRepository.getById(dto.getStatusId()));
        shipment.setArrival(dto.getArrival());
        shipment.setDeparture(dto.getArrival());
        return shipment;
    }
}
