package com.example.demo.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EditUserDto {

    private int id;

    @NotNull(message = "First name should not be null")
    @Size(min = 2, max = 100, message = "First name should be between 2 and 100")
    private String firstName;

    @NotNull(message = "Last name should not be null")
    @Size(min = 2, max = 100, message = "Last name should be between 2 and 100")
    private String lastName;

    @NotNull(message = "Username name should not be null")
    @Size(min = 4, max = 100, message = "Username should be between 4 and 100")
    private String username;

    @NotNull(message = "Password should not be null")
    @Size(min = 2, max = 50, message = "Password should be between 2 and 50")
    private String password;

    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 4 , max = 24, message = "Password must be between 4 and 24 symbols.")
    private String repeatPassword;

    @NotNull(message = "Email should not be null")
    @Email
    private String email;

    @JsonIgnore
    @NotNull
    private int roleId;

    private AddressDto address;

    private int addressId;

    public EditUserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }
}
