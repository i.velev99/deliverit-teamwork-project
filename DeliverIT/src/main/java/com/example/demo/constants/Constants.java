package com.example.demo.constants;

public class Constants {
    public static final String AUTHORIZATION_ERROR_MESSAGE = "The requested resource requires authorization";
    public final static String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String INVALID_USERNAME = "Invalid username";
    public static final String USER_EXISTS_ERROR_MESSAGE = "User with this username or email already exists.";
    public static final String CUSTOMER_ATTACHED_ERROR_MESSAGE = "Customer is attached to a parcel";
    public static final String UNAUTHORIZED_CREATOR_ERROR_MESSAGE = "Only the creator can modify the customer.";
    public static final int STATUS_PREPARING = 1;
    public static final int STATUS_ON_THE_WAY = 2;
    public static final int STATUS_COMPLETED = 3;
    public static final int CUSTOMER_ROLE = 2;
    public static final int EMPLOYEE_ROLE = 1;
    public static final String NO_PARCELS_ERROR_MESSAGE = "There aren't any parcels.";
    public static final String PARCEL_CREATE_ERROR_MESSAGE = "Cannot create a parcel, because shipment is not Preparing.";
    public static final String ON_THE_WAY_STATUS = "On the way";
    public static final String SHIPMENT_ON_THE_WAY_ERROR_MESSAGE = "The shipment is on the way.";
    public static final String SHIPMENT_STATUS_ERROR_MESSAGE = "Cannot create shipment with status different from Preparing.";
    public static final String SHIPMENT_PARCELS_ATTACHED_ERROR_MESSAGE = "Shipment has parcels attached.";
    public static final String NO_INCOMING_SHIPMENTS_ERROR_MESSAGE = "There are no incoming shipments.";
    public static final String NO_NEXT_SHIPMENT_ERROR_MESSAGE = "There is no next shipment.";
    public static final String NO_SHIPMENTS_ERROR_MESSAGE = "There aren't any shipments.";
    public static final String WAREHOUSE_PARCELS_ATTACHED_ERROR_MESSAGE = "Warehouse has parcels attached.";
    public static final String EMPLOYEE_PARCELS_ERROR_MESSAGE = "Only an employee can modify parcels.";
    public static final String CUSTOMER_SHIPMENTS_ERROR_MESSAGE = "Only a customer can see shipments.";
    public static final String NEXT_SHIPMENT_ERROR_MESSAGE = "Only employees can see the next shipment to arrive.";
    public static final String SHIPMENTS_EMPLOYEES_ERROR_MESSAGE = "Only employees can modify shipments.";
    public static final String USER_USERNAME_ERROR_MESSAGE = "User with this username already exists.";
    public static final String USER_EMAIL_ERROR_MESSAGE = "User with this email already exists.";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";

}
