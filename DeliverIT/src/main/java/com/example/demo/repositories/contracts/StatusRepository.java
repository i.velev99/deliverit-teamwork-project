package com.example.demo.repositories.contracts;

import com.example.demo.models.Status;

import java.util.List;

public interface StatusRepository {
    List<Status> getAll();

    com.example.demo.models.Status getById(int id);
}
