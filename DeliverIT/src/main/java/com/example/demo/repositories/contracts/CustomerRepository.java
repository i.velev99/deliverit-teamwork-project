package com.example.demo.repositories.contracts;

import com.example.demo.models.Address;
import com.example.demo.models.Customer;
import com.example.demo.models.Parcel;
import com.example.demo.models.User;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository {
    List<Customer> filterCustomers(Optional<String> firstName, Optional<String> lastName, Optional<String> email);
    Customer getById(int id);
    void create(Customer customer, Address address);
    void update(Customer customer, Address address);
    void delete(int id);

    Customer getByUsername(String username);

    List<Parcel> ownedParcelsByStatus(User user, int status);

    Customer getByEmail(String email);

}
