package com.example.demo.repositories.contracts;

import com.example.demo.models.Address;

import java.util.List;

public interface AddressRepository {
    List<Address> getAll();

    Address getById(int id);

}
