package com.example.demo.repositories.contracts;

import com.example.demo.models.Address;
import com.example.demo.models.Warehouse;

import java.util.List;

public interface WarehouseRepository {
    List<Warehouse> getAll();
    Warehouse getById(int id);
    void create(Warehouse warehouse, Address address);
    void update(Warehouse warehouse, Address address);
    void delete(int id);
}
