package com.example.demo.repositories.contracts;

import com.example.demo.models.City;

import java.util.List;

public interface CityRepository {
    List<City> getAll();

    City getById(int id);
}
