package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from Employee ", User.class);
            Query<User> secondQuery = session.createQuery("from Customer ", User.class);
            List<User> users = new ArrayList<>(query.list());
            users.addAll(secondQuery.list());
            return users;
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Customer where id = :id";
            Query<User> query = session.createQuery(queryString, User.class);
            query.setParameter("id", id);
            List<User> customers = query.list();
            if (customers.size() == 0) {
                queryString = "From Employee where id = :id";
                query = session.createQuery(queryString, User.class);
                query.setParameter("id", id);
                List<User> employees = query.list();
                if(employees.size() == 0) {
                    throw new EntityNotFoundException("User", id);
                }
                return employees.get(0);
            }
            return customers.get(0);
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Customer where username = :username";
            Query<User> query = session.createQuery(queryString, User.class);
            query.setParameter("username", username);
            List<User> customers = query.list();
            if (customers.size() == 0) {
                queryString = "From Employee where username = :username";
                query = session.createQuery(queryString, User.class);
                query.setParameter("username", username);
                List<User> employees = query.list();
                if(employees.size() == 0) {
                    throw new EntityNotFoundException("User", "username", username);
                }
                return employees.get(0);
            }
            return customers.get(0);
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Customer where email = :email";
            Query<User> query = session.createQuery(queryString, User.class);
            query.setParameter("email", email);
            List<User> customers = query.list();
            if (customers.size() == 0) {
                queryString = "From Employee where email = :email";
                query = session.createQuery(queryString, User.class);
                query.setParameter("email", email);
                List<User> employees = query.list();
                if(employees.size() == 0) {
                    throw new EntityNotFoundException("User", "email", email);
                }
                return employees.get(0);
            }
            return customers.get(0);
        }
    }
}
