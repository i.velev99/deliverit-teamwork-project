package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Address;
import com.example.demo.models.User;
import com.example.demo.models.Warehouse;
import com.example.demo.repositories.contracts.WarehouseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.UnexpectedRollbackException;

import java.util.List;

@Repository
public class WarehouseRepositoryImpl implements WarehouseRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public WarehouseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Warehouse> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Query<Warehouse> query = session.createQuery("from Warehouse as w order by w.address.city.country.name," +
                    " w.address.city.name , w.address.address", Warehouse.class);
            return query.getResultList();
        }
    }

    @Override
    public Warehouse getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Warehouse where id = :id";
            Query<Warehouse> query = session.createQuery(queryString, Warehouse.class);
            query.setParameter("id", id);
            List<Warehouse> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Warehouse", id);
            }
            return users.get(0);
        }
    }

    @Override
    public void create(Warehouse warehouse, Address address) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(address);
            warehouse.setAddress(address);
            session.save(warehouse);
            transaction.commit();
        }catch (RuntimeException e) {
            transaction.rollback();
            throw new UnexpectedRollbackException(e.getMessage());
        }
    }

    @Override
    public void update(Warehouse warehouse, Address address) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(address);
            session.update(warehouse);
            transaction.commit();
        }catch (RuntimeException e) {
            transaction.rollback();
            throw new UnexpectedRollbackException(e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        Warehouse warehouseToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(warehouseToDelete);
            session.getTransaction().commit();
        }
    }
}
