package com.example.demo.repositories.contracts;

import com.example.demo.models.Category;

import java.util.List;

public interface CategoryRepository {
    List<Category> getAll();
    Category getById(int id);
}
