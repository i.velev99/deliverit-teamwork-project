package com.example.demo.repositories.contracts;

import com.example.demo.models.Employee;
import com.example.demo.models.User;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {
    List<Employee> filterEmployees( Optional<String> firstName, Optional<String> lastName, Optional<String> email);

    Employee getById(int id);

    void create(Employee employee);

    void update(Employee employee);

    void delete(int id);

    Employee getByUsername(String username);

    List<Employee> getAll();

    Employee getByEmail(String email);
}
