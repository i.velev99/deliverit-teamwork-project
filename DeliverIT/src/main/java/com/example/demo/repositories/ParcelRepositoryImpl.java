package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Parcel;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.CategoryRepository;
import com.example.demo.repositories.contracts.CustomerRepository;
import com.example.demo.repositories.contracts.ParcelRepository;
import com.example.demo.repositories.contracts.WarehouseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class ParcelRepositoryImpl implements ParcelRepository {

    private final SessionFactory sessionFactory;
    private final CategoryRepository categoryRepository;
    private final CustomerRepository customerRepository;
    private final WarehouseRepository warehouseRepository;

    @Autowired
    public ParcelRepositoryImpl(SessionFactory sessionFactory, CategoryRepository categoryRepository, CustomerRepository customerRepository, WarehouseRepository warehouseRepository) {
        this.sessionFactory = sessionFactory;
        this.categoryRepository = categoryRepository;
        this.customerRepository = customerRepository;
        this.warehouseRepository = warehouseRepository;
    }

    @Override
    public List<Parcel> filterParcels(Optional<Double> minWeight, Optional<Double> maxWeight, Optional<Integer> customerId,
                                      Optional<Integer> warehouseId, Optional<Integer> categoryId ,
                                      Optional<String> sortParam) {

        categoryId.ifPresent(categoryRepository::getById);
        customerId.ifPresent(customerRepository::getById);
        warehouseId.ifPresent(warehouseRepository::getById);

        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Parcel as parcel where parcel.weight >= :minWeight " +
                    "and parcel.weight <= :maxWeight ";
            if (customerId.isPresent()) {
                queryString += "and customer.id = :customerId ";
            }
            if (warehouseId.isPresent()) {
                queryString += "and warehouse.id = :warehouseId ";
            }
            if (categoryId.isPresent()) {
                queryString += "and category.id = :categoryId ";
            }
            if (sortParam.isPresent()) {

                if (sortParam.get().equalsIgnoreCase("weight")) {
                    queryString += "order by parcel.weight ";
                }
                else if (sortParam.get().equalsIgnoreCase("arrival")) {
                    queryString += "order by parcel.shipment.arrival ";
                }
                else if (sortParam.get().equalsIgnoreCase("weightAndArrival")) {
                    queryString += "order by parcel.weight,parcel.shipment.arrival ";
                }
                else if (sortParam.get().equalsIgnoreCase("arrivalAndWeight")) {
                    queryString += "order by parcel.shipment.arrival,parcel.weight ";
                }
            }

            Query<Parcel> query = session.createQuery(queryString, Parcel.class);
            query.setParameter("minWeight", minWeight.orElse(0.0));
            query.setParameter("maxWeight", maxWeight.orElse(Double.MAX_VALUE));
            customerId.ifPresent(integer -> query.setParameter("customerId", integer));
            warehouseId.ifPresent(integer -> query.setParameter("warehouseId", integer));
            categoryId.ifPresent(integer -> query.setParameter("categoryId", integer));
            return query.list();
        }
    }

    @Override
    public Parcel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Parcel where id = :id";
            Query<Parcel> query = session.createQuery(queryString, Parcel.class);
            query.setParameter("id", id);
            List<Parcel> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Parcel", id);
            }
            return users.get(0);
        }
    }

    @Override
    public void create(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.save(parcel);
        }
    }

    @Override
    public void update(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(parcel);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Parcel parcelToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(parcelToDelete);
            session.getTransaction().commit();
        }
    }

}
