package com.example.demo.repositories;


import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Status;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.StatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusRepositoryImpl implements StatusRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Status> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("from Status ", Status.class);
            return query.getResultList();
        }
    }

    @Override
    public Status getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Status where id = :id";
            Query<Status> query = session.createQuery(queryString, Status.class);
            query.setParameter("id", id);
            List<Status> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Status", id);
            }
            return users.get(0);
        }
    }
}
