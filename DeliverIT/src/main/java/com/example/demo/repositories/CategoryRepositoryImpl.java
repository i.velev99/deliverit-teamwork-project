package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Category;
import com.example.demo.models.Parcel;
import com.example.demo.repositories.contracts.CategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category ", Category.class);
            return query.getResultList();
        }
    }

    @Override
    public com.example.demo.models.Category getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Category where id = :id";
            Query<Category> query = session.createQuery(queryString, Category.class);
            query.setParameter("id", id);
            List<Category> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Category", id);
            }
            return users.get(0);
        }
    }
}
