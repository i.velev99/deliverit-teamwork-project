package com.example.demo.repositories.contracts;

import com.example.demo.models.Parcel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ParcelRepository {
    Parcel getById(int id);

    void create(Parcel parcel);

    void update(Parcel parcel);

    void delete(int id);

    List<Parcel> filterParcels(Optional<Double> minWeight, Optional<Double> maxWeight,
                               Optional<Integer> customerId,
                               Optional<Integer> warehouseId,
                               Optional<Integer> categoryId ,
                               Optional<String> sortParam);

}
