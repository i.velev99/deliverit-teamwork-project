package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Address;
import com.example.demo.models.Parcel;
import com.example.demo.repositories.contracts.AddressRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AddressRepositoryImpl implements AddressRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddressRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Address> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Address> query = session.createQuery("from Address", Address.class);
            return query.getResultList();
        }
    }

    @Override
    public Address getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Address where id = :id";
            Query<Address> query = session.createQuery(queryString, Address.class);
            query.setParameter("id", id);
            List<Address> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Address", id);
            }
            return users.get(0);
        }
    }

}