package com.example.demo.repositories.contracts;

import com.example.demo.models.*;
import org.springframework.http.HttpHeaders;

import java.util.List;
import java.util.Optional;

public interface ShipmentRepository {
    Shipment getById(int id);

    void create(Shipment shipment);

    void update(Shipment shipment);

    void delete(int id);

    List<Shipment> filterShipments(Optional<Integer> warehouseId, Optional<Integer> customerId);

    List<Shipment> getShipmentsByStatus(int statusId);

    Shipment nextShipmentToArrive();


    List<Shipment> shipmentsBelongingToCustomer(User user);
}
