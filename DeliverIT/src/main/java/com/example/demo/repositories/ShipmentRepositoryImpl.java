package com.example.demo.repositories;

import com.example.demo.exceptions.EmptyEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.*;
import com.example.demo.repositories.contracts.CustomerRepository;
import com.example.demo.repositories.contracts.ShipmentRepository;
import com.example.demo.repositories.contracts.WarehouseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.example.demo.constants.Constants.*;

@Repository
public class ShipmentRepositoryImpl implements ShipmentRepository {

    private final SessionFactory sessionFactory;
    private final CustomerRepository customerRepository;
    private final WarehouseRepository warehouseRepository;

    @Autowired
    public ShipmentRepositoryImpl(SessionFactory sessionFactory, CustomerRepository customerRepository, WarehouseRepository warehouseRepository) {
        this.sessionFactory = sessionFactory;
        this.customerRepository = customerRepository;
        this.warehouseRepository = warehouseRepository;
    }


    @Override
    public List<Shipment> filterShipments(Optional<Integer> warehouseId, Optional<Integer> customerId) {

        customerId.ifPresent(customerRepository::getById);
        warehouseId.ifPresent(warehouseRepository::getById);


        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Shipment where 1=1 ";
            if (customerId.isPresent()) {
                queryString += "and customer.id = :customerId ";
            }
            if (warehouseId.isPresent()) {
                queryString += "and warehouse.id = :warehouseId ";
            }
            Query<Shipment> query = session.createQuery(queryString, Shipment.class);
            customerId.ifPresent(integer -> query.setParameter("customerId", integer));
            warehouseId.ifPresent(integer -> query.setParameter("warehouseId", integer));
            return query.list();
        }
    }

    @Override
    public List<Shipment> getShipmentsByStatus(int statusId) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Shipment where status.id = :statusId";
            Query<Shipment> query = session.createQuery(queryString, Shipment.class);
            query.setParameter("statusId", statusId);

            if (query.list().isEmpty()) {
                throw new EmptyEntityException(NO_INCOMING_SHIPMENTS_ERROR_MESSAGE);
            }
            return query.list();
        }
    }

    @Override
    public Shipment nextShipmentToArrive() {

        try (Session session = sessionFactory.openSession()) {
            int onTheWay = 2;
            String queryString = "from Shipment where status.id = :onTheWay " +
                    "and arrival > current_date " +
                    "order by arrival asc ";
            Query<Shipment> query = session.createQuery(queryString, Shipment.class);
            query.setParameter("onTheWay", onTheWay);
            query.setMaxResults(1);
            if (query.getSingleResult() == null) {
                throw new EmptyEntityException(NO_NEXT_SHIPMENT_ERROR_MESSAGE);
            }

            return query.getSingleResult();
        }
    }

    @Override
    public List<Shipment> shipmentsBelongingToCustomer(User user) {
        String username = user.getUsername();
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Shipment as s where 1=1 " +
                    "and id in (select shipment.id from Parcel where customer.username = :username)";
            Query<Shipment> query = session.createQuery(queryString, Shipment.class);
            query.setParameter("username", username);

            if (query.list().isEmpty()) {
                throw new EmptyEntityException(NO_SHIPMENTS_ERROR_MESSAGE);
            }
            return query.list();
        }
    }

    @Override
    public Shipment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Shipment where id = :id";
            Query<Shipment> query = session.createQuery(queryString, Shipment.class);
            query.setParameter("id", id);
            List<Shipment> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Shipment", id);
            }
            return users.get(0);
        }
    }

    @Override
    public void create(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(shipment);
        }
    }

    @Override
    public void update(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Shipment shipmentToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(shipmentToDelete);
            session.getTransaction().commit();
        }
    }
}
