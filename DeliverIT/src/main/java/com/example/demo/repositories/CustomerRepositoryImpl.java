package com.example.demo.repositories;

import com.example.demo.exceptions.EmptyEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.*;
import com.example.demo.repositories.contracts.CustomerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.UnexpectedRollbackException;

import java.util.List;
import java.util.Optional;

import static com.example.demo.constants.Constants.*;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CustomerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Customer> filterCustomers(Optional<String> firstName, Optional<String> lastName,
                                          Optional<String> email) {

        try (Session session = sessionFactory.openSession()) {

            String queryString = "from Customer where 1=1 " +
                    "and role.id = :customerRole ";
            if (firstName.isPresent()) {
                queryString += "and firstName like concat('%',:firstname,'%') ";
            }
            if (lastName.isPresent()) {
                queryString += "and lastName like concat('%',:lastname,'%') ";
            }
            if (email.isPresent()) {
                queryString += "and email like concat('%',:email,'%') ";
            }
            Query<Customer> query = session.createQuery(queryString, Customer.class);
            query.setParameter("customerRole", CUSTOMER_ROLE);
            firstName.ifPresent(first -> query.setParameter("firstname", first));
            lastName.ifPresent(last -> query.setParameter("lastname", last));
            email.ifPresent(customerEmail -> query.setParameter("email", customerEmail));
            return query.list();

        }

    }

    @Override
    public List<Parcel> ownedParcelsByStatus(User user, int status) {
        String username = user.getUsername();
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Parcel as p where p.customer.username = :username " +
                    "and p.shipment.status.id = :status";
            Query<Parcel> query = session.createQuery(queryString, Parcel.class);
            query.setParameter("username", username);
            query.setParameter("status", status);

            if (query.getResultList().isEmpty()) {
                throw new EmptyEntityException(NO_PARCELS_ERROR_MESSAGE);
            }

            return query.getResultList();
        }
    }

    @Override
    public Customer getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Integer customerRole = 2;
            String queryString = "From Customer where email= :email" +
                    " and role.id = :customerRole";
            Query<Customer> query = session.createQuery(queryString, Customer.class);
            query.setParameter("email", email);
            query.setParameter("customerRole", customerRole);
            List<Customer> customers = query.list();
            if (customers.size() == 0) {
                throw new EntityNotFoundException("Customer", "email", email);
            }
            return customers.get(0);
        }
    }

    @Override
    public Customer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Customer where id = :id";
            Query<Customer> query = session.createQuery(queryString, Customer.class);
            query.setParameter("id", id);
            List<Customer> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Customer", id);
            }
            return users.get(0);
        }
    }

    @Override
    public void create(Customer customer, Address address) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(address);
            customer.setAddress(address);
            session.save(customer);
            transaction.commit();
        } catch (RuntimeException e) {
            transaction.rollback();
            throw new UnexpectedRollbackException(e.getMessage());
        }
    }

    @Override
    public void update(Customer customer, Address address) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(address);
            session.update(customer);
            transaction.commit();
        } catch (RuntimeException e) {
            transaction.rollback();
            throw new UnexpectedRollbackException(e.getMessage());
        }
    }

//    public void update(Customer customer, Address address) {
//        Transaction tx = null;
//        try (Session session = sessionFactory.openSession()) {
//            tx = session.beginTransaction();
//            session.update(address);
//            customer.setAddress(address);
//            session.update(customer);
//            session.getTransaction().commit();
//        } catch (RuntimeException e) {
//            tx.rollback();
//            throw new RuntimeException(e.toString());
//        }
//
//    }


    @Override
    public void delete(int id) {
        Customer customerToDelete = getById(id);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(customerToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public Customer getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Integer customerRole = 2;
            String queryString = "From Customer where username= :username" +
                    " and role.id = :customerRole";
            Query<Customer> query = session.createQuery(queryString, Customer.class);
            query.setParameter("username", username);
            query.setParameter("customerRole", customerRole);
            List<Customer> customers = query.list();
            if (customers.size() == 0) {
                throw new EntityNotFoundException("Customer", "username", username);
            }
            return customers.get(0);
        }
    }
}
