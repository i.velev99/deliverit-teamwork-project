package com.example.demo.repositories.contracts;

import com.example.demo.models.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getById(int id);
}
