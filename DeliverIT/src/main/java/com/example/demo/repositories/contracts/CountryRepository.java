package com.example.demo.repositories.contracts;

import com.example.demo.models.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAll();

    Country getById(int id);

    Country getByName(String name);
}
