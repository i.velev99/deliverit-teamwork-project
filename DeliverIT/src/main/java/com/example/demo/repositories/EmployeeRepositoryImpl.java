package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.Employee;
import com.example.demo.models.Parcel;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.EmployeeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.example.demo.constants.Constants.CUSTOMER_ROLE;
import static com.example.demo.constants.Constants.EMPLOYEE_ROLE;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public EmployeeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Employee> filterEmployees( Optional<String> firstName,
                                           Optional<String> lastName,
                                           Optional<String> email) {

        try (Session session = sessionFactory.openSession()) {

            String queryString = "from Employee where 1=1 "+
                    "and role.id = :employeeRole ";
            if (firstName.isPresent()) {
                queryString += "and firstName like concat('%',:firstname,'%')";
            }
            if (lastName.isPresent()) {
                queryString += "and lastName like concat('%',:lastname,'%') ";
            }
            if (email.isPresent()) {
                queryString += "and email like concat('%',:email,'%') ";
            }
            Query<Employee> query = session.createQuery(queryString, Employee.class);
            query.setParameter("employeeRole", EMPLOYEE_ROLE);
            firstName.ifPresent(first -> query.setParameter("firstname", first));
            lastName.ifPresent(last -> query.setParameter("lastname", last));
            email.ifPresent(customerEmail -> query.setParameter("email", customerEmail));

            return query.list();

        }

    }

    @Override
    public List<Employee> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Employee> query = session.createQuery("from Employee", Employee.class);
            return query.list();

        }
    }

    @Override
    public Employee getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Integer employeeRole = 1;
            String queryString = "From Employee as e  where e.email= :email" +
                    " and e.role.id = :employeeRole";
            Query<Employee> query = session.createQuery(queryString, Employee.class);
            query.setParameter("email", email);
            query.setParameter("employeeRole", employeeRole);
            List<Employee> employees = query.list();
            if (employees.size() == 0) {
                throw new EntityNotFoundException("Employee", "email", email);
            }
            return employees.get(0);
        }
    }

    @Override
    public Employee getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Employee where id = :id";
            Query<Employee> query = session.createQuery(queryString, Employee.class);
            query.setParameter("id", id);
            List<Employee> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Employee", id);
            }
            return users.get(0);
        }
    }

    @Override
    public void create(Employee employee) {
        try (Session session = sessionFactory.openSession()) {
            session.save(employee);
        }
    }

    @Override
    public void update(Employee employee) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(employee);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Employee employeeToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(employeeToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public Employee getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Integer employeeRole = 1;
            String queryString = "From Employee as e  where e.username= :username" +
                    " and e.role.id = :employeeRole";
            Query<Employee> query = session.createQuery(queryString, Employee.class);
            query.setParameter("username", username);
            query.setParameter("employeeRole", employeeRole);
            List<Employee> employees = query.list();
            if (employees.size() == 0) {
                throw new EntityNotFoundException("Employee", "name", username);
            }
            return employees.get(0);
        }
    }
}
