//package com.example.demo.repositories;
//
//import com.example.demo.exceptions.EntityNotFoundException;
//import com.example.demo.models.Country;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public class CountryRepositoryImpl extends MainRepositoryImpl<Country> implements CountryRepository {
//
//    private final SessionFactory sessionFactory;
//
//    @Autowired
//    public CountryRepositoryImpl(SessionFactory sessionFactory) {
//        super(sessionFactory);
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public Class<Country> getSource() {
//        return Country.class;
//    }
//
//    @Override
//    public Country getByName(String name) {
//        try (Session session = sessionFactory.openSession()) {
//            String queryString = "from Country where name = :name";
//            Query<Country> query = session.createQuery(queryString, Country.class);
//            query.setParameter("name", name);
//
//            List<Country> result = query.list();
//            if (result.size() == 0) {
//                throw new EntityNotFoundException("Country", "name", name);
//            }
//
//            return result.get(0);
//        }
//    }
//}
//


package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Country;
import com.example.demo.models.Parcel;
import com.example.demo.repositories.contracts.CountryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Country> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country" , Country.class);
            return query.getResultList();
        }
    }

    @Override
    public Country getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Country where id = :id";
            Query<Country> query = session.createQuery(queryString, Country.class);
            query.setParameter("id", id);
            List<Country> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("Country", id);
            }
            return users.get(0);
        }
    }

    @Override
    public Country getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name = :name", Country.class);
            query.setParameter("name", name);

            List<Country> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Country", "name", name);
            }

            return result.get(0);
        }
    }

}
