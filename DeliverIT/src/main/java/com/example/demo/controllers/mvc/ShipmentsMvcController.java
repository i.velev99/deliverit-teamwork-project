package com.example.demo.controllers.mvc;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.EditShipmentDto;
import com.example.demo.models.dto.ShipmentDto;
import com.example.demo.repositories.contracts.StatusRepository;
import com.example.demo.services.contracts.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.example.demo.controllers.mvc.AuthenticationMvcController.EMPLOYEE_ROLE;

@Controller
@RequestMapping("/shipments")
public class ShipmentsMvcController {

    private final ShipmentService service;
    private final StatusRepository statusRepository;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public ShipmentsMvcController(ShipmentService service,
                                  StatusRepository statusRepository,
                                  AuthenticationHelper authenticationHelper,
                                  ModelMapper modelMapper) {
        this.service = service;
        this.statusRepository = statusRepository;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("statuses")
    public List<Status> getAllCategories() {
        return statusRepository.getAll();
    }


    @GetMapping
    public String showShipments(Model model, HttpSession session) {
        List<Shipment> shipments = service.filterShipments(Optional.empty(), Optional.empty());
        model.addAttribute("shipments", shipments);
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "shipments";
    }

    @GetMapping("/next-shipment")
    public String showNextShipmentToArrive(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            return "error";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
//            model.addAttribute("currentUser", null);
        }
        Shipment shipment = service.nextShipmentToArrive(user);
        model.addAttribute("nextShipment", shipment);
        return "next-shipment";
    }

    @GetMapping("/{id}")
    public String showSingleShipment(@PathVariable int id, Model model, HttpSession session) {
        try {
            Shipment shipment= service.getShipmentById(id);
            User currentUser = authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("shipment", shipment);
            return "shipment";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}/parcels")
    public String showNextShipmentToArrive(@PathVariable int id, Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            Shipment shipment = service.getShipmentById(id);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("shipment", shipment);
            return "shipment-parcels";
        } catch (UnauthorizedOperationException e) {
            return "error";
        }
    }

    @GetMapping("/create")
    public String showCreateShipmentPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session,"Employee");
            model.addAttribute("createShipmentDto", new ShipmentDto());
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "create-shipment";
    }

    @PostMapping("/create")
    public String handleCreateShipment(@Valid @ModelAttribute("createShipmentDto") ShipmentDto dto,
                                     BindingResult bindingResult,
                                     HttpSession session,
                                     Model model) {
        if(bindingResult.hasErrors()) {
            return "error";
        }

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session,"Employee");
            Shipment shipment = modelMapper.fromDto(dto);
            service.create(shipment, currentUser);
            return "redirect:/shipments";
        } catch (UnsupportedOperationException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

    @GetMapping("/{id}/edit")
    public String showEditParcelShipment(@PathVariable int id, HttpSession session, Model model) {

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Shipment shipment = service.getShipmentById(id);
            EditShipmentDto shipmentDto = modelMapper.fromShipment(shipment);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("editShipmentDto", shipmentDto);
            return "edit-shipment";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

    @PostMapping("/{id}/edit")
    public String handleEditShipment(Model model,
                                   HttpSession session,
                                   @PathVariable int id,
                                   @Valid @ModelAttribute("editShipmentDto") EditShipmentDto dto,
                                   BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "edit-shipment";
        }

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Shipment getById = service.getShipmentById(id);
            Shipment shipment = modelMapper.fromDtoShipment(dto, id);
            service.update(shipment, currentUser);
        } catch (UnsupportedOperationException | EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "redirect:/shipments";
    }

    @PostMapping("/{id}/delete")
    public String handleDeleteShipment(Model model,
                                     HttpSession session,
                                     @PathVariable int id){

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Shipment shipment = service.getShipmentById(id);
            service.delete(id,currentUser);
            return "redirect:/shipments";
        } catch (UnsupportedOperationException | EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

}
