package com.example.demo.controllers.rest;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.EmptyEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.ShipmentDto;
import com.example.demo.services.contracts.ShipmentService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.annotation.HttpConstraint;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/shipments")
public class ShipmentController {

    private final ShipmentService service;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ShipmentController(ShipmentService service, ModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Filter Shipments by minWeight  warehouseId / customerId ",
            response = Shipment.class)
    private List<Shipment> filterShipments(@RequestParam(required = false) Optional<Integer> warehouseId,
                                           @RequestParam(required = false) Optional<Integer> customerId) {

        try {
            return service.filterShipments(warehouseId, customerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get Shipments by Id",
            response = Shipment.class)
    private Shipment getShipmentById(@PathVariable int id) {
        try {
            return service.getShipmentById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    //Get Shipments with OnTheWay Status
    @GetMapping("/on-the-way")
    @ApiOperation(value = "Get All Shipments that are On The Way",
            notes = "Only accessible by employees",
            response = Shipment.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    public List<Shipment> getOnTheWayShipments(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        try {
            return service.getOnTheWayShipments(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EmptyEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/next-shipment")
    @ApiOperation(value = "Get the Next Shipment To Arrive",
            notes = "Only accessible by employees",
            response = Shipment.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    public Shipment nextShipmentToArrive(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        try {
            return service.nextShipmentToArrive(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EmptyEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/my-shipments")
    @ApiOperation(value = "Get all the Shipments with belonging Parcels of a User",
            notes = "Only accessible by Users",
            response = Shipment.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    public List<Shipment> shipmentsBelongingToCustomer(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return service.shipmentsBelongingToCustomer(user);
        }catch (EmptyEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Create Shipment",
            notes = "Only accessible by Employee",
            response = Shipment.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Shipment create(@RequestHeader HttpHeaders header, @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Shipment shipment = new Shipment();
            modelMapper.dtoToObject(shipmentDto, shipment);
            service.create(shipment, user);
            return shipment;
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Shipment",
            notes = "Only accessible by Employee",
            response = Shipment.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Shipment update(@RequestHeader HttpHeaders header, @PathVariable int id, @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Shipment shipment = modelMapper.fromDto(shipmentDto, id);
            service.update(shipment, user);
            return shipment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Shipment",
            notes = "Only accessible by Employee",
            response = Shipment.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Shipment delete(@RequestHeader HttpHeaders header, @PathVariable int id) {
        try {
           User user = authenticationHelper.tryGetUser(header);
            Shipment shipmentToBeDeleted = getShipmentById(id);
            service.delete(id, user);
            return shipmentToBeDeleted;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}

