package com.example.demo.controllers;

import com.example.demo.exceptions.AuthenticationFailureException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.Customer;
import com.example.demo.models.Employee;
import com.example.demo.models.User;
import com.example.demo.services.contracts.CustomerService;
import com.example.demo.services.contracts.EmployeeService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

import static com.example.demo.constants.Constants.*;

@Component
public class AuthenticationHelper {

    private final UserService userService;

    @Autowired
    public AuthenticationHelper( UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        checkAuthorization(headers);
        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_USERNAME);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUserUsername = (String) session.getAttribute("currentUser");

        if (currentUserUsername == null) {
            throw new UnauthorizedOperationException("No logged in user.");
        }

        try {
            return userService.getByUsername(currentUserUsername);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("No logged in user.");
        }
    }

    private void checkAuthorization(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {

            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    AUTHORIZATION_ERROR_MESSAGE);
        }
    }

    public User verifyAuthorization(HttpSession session, String role) {
        User user = tryGetUser(session);

        if (!user.getRole().getName().equalsIgnoreCase(role)) {
            throw new UnauthorizedOperationException("User does not have the required authorization.");
        }
        return user;
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByUsername(username);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }


}
