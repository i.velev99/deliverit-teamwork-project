package com.example.demo.controllers.rest;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.*;
import com.example.demo.models.*;
import com.example.demo.models.dto.AddressDto;
import com.example.demo.models.dto.CustomerDto;
import com.example.demo.services.contracts.CustomerService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {

    private final CustomerService customerService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CustomerController(CustomerService customerService, ModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.customerService = customerService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    //FILTER
    @GetMapping
    @ApiOperation(value = "Filter Customer by firstName / lastName / email",
            response = Customer.class)
    public List<Customer> filterCustomers(@RequestParam(required = false) Optional<String> firstName,
                                          @RequestParam(required = false) Optional<String> lastName,
                                          @RequestParam(required = false) Optional<String> email) {
        try {
            return customerService.filterCustomers(firstName, lastName, email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/incoming-parcels")
    @ApiOperation(value = "Get the incoming parcels for a given User",
            response = Customer.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    public List<Parcel> incomingParcels(@RequestHeader HttpHeaders headers) {
        try{
            User user = authenticationHelper.tryGetUser(headers);
            return customerService.incomingParcels(user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EmptyEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/past-parcels")
    @ApiOperation(value = "Get the past parcels for a given User",
            response = Customer.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")

    public List<Parcel> pastParcels(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return customerService.pastParcels(user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }catch (EmptyEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "Search Customer by Id",
            response = Customer.class)
    public Customer getById(@PathVariable int id) {
        try {
            return customerService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search/{username}")
    @ApiOperation(value = "Search Customer by Username",
            response = Customer.class)
    public Customer getByUsername(@PathVariable String username) {
        try {
            return customerService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Create User",
    response = Customer.class)
    private Customer create(@Valid @RequestBody CustomerDto customerDto) {
        try {
            Customer customer = new Customer();
            Address address = new Address();
            modelMapper.dtoToObject(customerDto, customer);
            AddressDto addressDto = customerDto.getAddress();
            modelMapper.dtoToObject(addressDto, address);
            customerService.create(customer, address);
            return customer;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnexpectedRollbackException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Customer",
            notes = "Only accessible by the creator",
            response = Customer.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Customer update(@RequestHeader HttpHeaders headers,
                            @PathVariable int id,
                            @Valid @RequestBody CustomerDto customerDto) {
        try {
            User creator = authenticationHelper.tryGetUser(headers);
            Customer customer = modelMapper.fromDto(customerDto, id);
            Address address = modelMapper.fromDto(customerDto.getAddress(), customer.getAddress().getId());
            customerService.update(customer, address, creator);
            return customer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnexpectedRollbackException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Customer",
            notes = "Only accessible by the creator",
            response = Customer.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Customer delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User creator = authenticationHelper.tryGetUser(headers);
            Customer customerToBeDeleted = customerService.getById(id);
            customerService.delete(id,creator);
            return customerToBeDeleted;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UserEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
