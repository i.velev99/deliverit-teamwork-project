package com.example.demo.controllers.rest;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Country;
import com.example.demo.models.Customer;
import com.example.demo.services.contracts.CountryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryController {

    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }


    @GetMapping
    @ApiOperation(value = "Get All Countries",
            response = Country.class)
    public List<Country> getCountries() {
        return countryService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Search Country by Id",
            response = Country.class)
    public Country getCountryById(@PathVariable int id) {
        try {
            return countryService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search/{name}")
    @ApiOperation(value = "Search Country by Name",
            response = Country.class)
    public Country getCountryByName(@PathVariable String name) {
        try {
            return countryService.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
