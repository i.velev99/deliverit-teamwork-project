package com.example.demo.controllers.rest;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.ParcelDto;
import com.example.demo.services.contracts.ParcelService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/parcels")
public class ParcelController {

    private final ParcelService service;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ParcelController(ParcelService service, ModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Filter Parcels by minWeight / maxWeight / customerId / warehouseId / categoryId / sortParam ",
            response = Parcel.class)
    public List<Parcel> getParcels(@RequestParam(required = false) Optional<Double> minWeight,
                                   @RequestParam(required = false) Optional<Double> maxWeight,
                                   @RequestParam(required = false) Optional<Integer> customerId,
                                   @RequestParam(required = false) Optional<Integer> warehouseId,
                                   @RequestParam(required = false) Optional<Integer> categoryId,
                                   @RequestParam(required = false) Optional<String> sortParam) {
        try {
            return service.filterParcels(minWeight, maxWeight, customerId, warehouseId, categoryId,
                    sortParam);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get Parcel by Id ",
            response = Parcel.class)
    public Parcel getParcel(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Create Parcel",
            notes = "Only accessible by employees",
            response = Parcel.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Parcel create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ParcelDto parcelDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Parcel parcel = new Parcel();
            modelMapper.dtoToObject(parcelDto, parcel);
            service.create(parcel, user);
            return parcel;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Parcel",
            notes = "Only accessible by employees",
            response = Parcel.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Parcel update(@RequestHeader HttpHeaders headers,
                          @PathVariable int id,
                          @Valid @RequestBody ParcelDto parcelDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Parcel parcelToUpdate = modelMapper.fromDto(parcelDto, id);
            service.update(parcelToUpdate, user);
            return parcelToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Parcel ",
            notes = "Only accessible by other employees",
            response = Parcel.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Parcel delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Parcel parcelToDelete = service.getById(id);
            service.delete(id, user);
            return parcelToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
