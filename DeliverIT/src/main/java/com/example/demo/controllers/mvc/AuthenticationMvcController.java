package com.example.demo.controllers.mvc;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.AuthenticationFailureException;
import com.example.demo.exceptions.DuplicateEntityException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.*;
import com.example.demo.services.contracts.CityService;
import com.example.demo.services.contracts.CountryService;
import com.example.demo.services.contracts.CustomerService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping
public class AuthenticationMvcController {
    public static final String EMPLOYEE_ROLE = "Employee";
    public static final String CUSTOMER_ROLE = "Customer";
    private final UserService userService;
    private final CustomerService customerService;
    private final CityService cityService;
    private final CountryService countryService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public AuthenticationMvcController(UserService userService,
                                       CustomerService customerService,
                                       CityService cityService,
                                       CountryService countryService,
                                       AuthenticationHelper authenticationHelper,
                                       ModelMapper modelMapper) {
        this.userService = userService;
        this.customerService = customerService;
        this.cityService = cityService;
        this.countryService = countryService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("cities")
    public List<City> allCities() {
        return cityService.getAll();
    }

    @ModelAttribute("countries")
    public List<Country> allCountries() {
        return countryService.getAll();
    }

    @GetMapping("/login")
    public String showLoginPage(Model model, HttpSession session) {
        model.addAttribute("login", new LoginDto());

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }

        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterCustomerPage(Model model, HttpSession session) {
        model.addAttribute("registerCustomerDto", new RegisterCustomerDto());

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }

        return "register";
    }

    @PostMapping("/register")
    public String handleRegisterCustomer(@Valid @ModelAttribute("registerCustomerDto") RegisterCustomerDto dto,
                                         BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "register";
        }

        if(!dto.getPassword().equals(dto.getRepeatPassword())) {
            bindingResult.rejectValue("password", "password-error", "Passwords don't match.");
        }

        Customer customerToRegister = modelMapper.fromDto(dto);
        Address customerAddress = modelMapper.fromDto(dto.getAddress());
        try {
            customerService.create(customerToRegister, customerAddress);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
        }

        return "redirect:/login";
    }
}
