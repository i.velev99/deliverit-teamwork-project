package com.example.demo.controllers.rest;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.DuplicateEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.EmployeeDto;
import com.example.demo.services.contracts.EmployeeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public EmployeeController(EmployeeService employeeService, ModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.employeeService = employeeService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    //FILTER
    @GetMapping
    @ApiOperation(value = "Filter Employees by firstName / lastName / email",
            response = Employee.class)
    public List<Employee> filterEmployees(@RequestParam(required = false) Optional<String> firstName,
                                          @RequestParam(required = false) Optional<String> lastName,
                                          @RequestParam(required = false) Optional<String> email) {
        try {
            return employeeService.filterEmployees(firstName, lastName, email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get an Employee by Id",
            response = Employee.class)
    public Employee getById(@PathVariable int id) {
        try {
            return employeeService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search/{username}")
    @ApiOperation(value = "Get an Employee by Username",
            response = Employee.class)
    public Employee getByUsername(@PathVariable String username) {
        try {
            return employeeService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Create Employee",
            notes = "Only accessible by other employees",
            response = Employee.class)
    private Employee create(@Valid @RequestBody EmployeeDto employeeDto) {
        try {
            Employee employee = new Employee();
            modelMapper.dtoToObject(employeeDto,employee);
            employeeService.create(employee);
            return employee;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnexpectedRollbackException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Employee",
            notes = "Only accessible by the creator",
            response = Employee.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Employee update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody EmployeeDto employeeDto) {
        try {
            User creator = authenticationHelper.tryGetUser(headers);
            Employee employee = modelMapper.fromDto(employeeDto, id);
            employeeService.update(employee, creator);
            return employee;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnexpectedRollbackException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Employee",
            notes = "Only accessible by the creator",
            response = Employee.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Employee delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User creator = authenticationHelper.tryGetUser(headers);
            Employee customerToBeDeleted = employeeService.getById(id);
            employeeService.delete(id, creator);
            return customerToBeDeleted;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnexpectedRollbackException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
