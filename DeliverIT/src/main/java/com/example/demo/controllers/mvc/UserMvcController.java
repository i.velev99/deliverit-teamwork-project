package com.example.demo.controllers.mvc;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.DuplicateEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.EditUserDto;
import com.example.demo.repositories.contracts.AddressRepository;
import com.example.demo.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("profile")
public class UserMvcController {

    private final UserService userService;
    private final EmployeeService employeeService;
    private final CustomerService customerService;
    private final AddressRepository addressRepository;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final CityService cityService;
    private final CountryService countryService;

    @Autowired
    public UserMvcController(UserService userService,
                             EmployeeService employeeService,
                             CustomerService customerService,
                             AddressRepository addressRepository,
                             AuthenticationHelper authenticationHelper,
                             ModelMapper modelMapper,
                             CityService cityService,
                             CountryService countryService) {
        this.userService = userService;
        this.employeeService = employeeService;
        this.customerService = customerService;
        this.addressRepository = addressRepository;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.cityService = cityService;
        this.countryService = countryService;
    }

    @ModelAttribute("cities")
    public List<City> allCities() {
        return cityService.getAll();
    }

    @ModelAttribute("countries")
    public List<Country> allCountries() {
        return countryService.getAll();
    }

    @GetMapping("/{id}")
    public String showProfilePage(@PathVariable int id, HttpSession session, Model model) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            /* Allow access only if the user is employee or trying to see his own profile */
            if (!currentUser.getRole().getName().equals("Employee") && currentUser.getId() != (id)) {
                model.addAttribute("error", "Unauthorized operation");
                return "error";
            }
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        User user;
        try {
            user = userService.getById(id);
            if (user.getRole().getName().equals("Customer")) {
                Customer customer = customerService.getByUsername(user.getUsername());
                Address address = addressRepository.getById(customer.getAddress().getId());
                model.addAttribute("address", address);
            }
            model.addAttribute("user", user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "profile";
    }

    @GetMapping("/{id}/edit")
    public String showEditProfilePage(@PathVariable int id, HttpSession session, Model model) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            /* Allow access only if the user is trying to edit his own profile */
            if (currentUser.getId() != (id)) {
                model.addAttribute("error", "Unauthorized operation");
                return "error";
            }
            User user = userService.getById(id);
            EditUserDto dto = modelMapper.fromUser(user);

            if(user.getRole().getName().equals("Customer")) {
                Customer customer = customerService.getById(id);
                dto.setAddressId(customer.getAddress().getId());
            }

            model.addAttribute("currentUser", currentUser);
            model.addAttribute("editUserDto", dto);
            return "edit";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }


    @PostMapping("/{id}/edit")
    public String handleEditUser(Model model,
                                 HttpSession session,
                                 @PathVariable int id,
                                 @Valid @ModelAttribute("editUserDto") EditUserDto dto,
                                 BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "edit";
        }
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
            /* Allow access only if the user is trying to edit his own profile */
            if (currentUser.getId() != id) {
                model.addAttribute("error", "Unauthorized operation");
                return "error";
            }
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }


        try {
            User user = userService.getById(id);
            if(user.getRole().getName().equals("Customer")) {
                Customer userToUpdate = modelMapper.fromDtoCustomer(dto, id);
                Address address = modelMapper.fromDto(dto, dto.getAddressId());
                customerService.update(userToUpdate, address, currentUser);
            } else if(user.getRole().getName().equals("Employee")) {
                Employee userToUpdate = modelMapper.fromDtoEmployee(dto, id);
                employeeService.update(userToUpdate, currentUser);
            }
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "duplicate_username", e.getMessage());
            bindingResult.rejectValue("email", "duplicate_email", e.getMessage());
            return "edit";
        }

        return "redirect:/profile/{id}";
    }

    @PostMapping("/{id}/delete")
    public String handleDeleteUser(Model model,
                                   HttpSession session,
                                   @PathVariable int id){

        try {
           User currentUser = authenticationHelper.tryGetUser(session);
            /* Allow access only if the user is trying to edit his own profile */
            if (currentUser.getId() != id) {
                model.addAttribute("error", "Unauthorized operation");
                return "error";
            }

            User user = userService.getById(id);
            if(user.getRole().getName().equals("Customer")) {
                customerService.delete(id, user);
            } else if(user.getRole().getName().equals("Employee")) {
                employeeService.delete(id, user);
            }

            return "redirect:/";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

}
