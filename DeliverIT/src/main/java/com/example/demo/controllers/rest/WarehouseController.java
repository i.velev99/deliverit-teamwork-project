package com.example.demo.controllers.rest;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.WarehouseDto;
import com.example.demo.services.contracts.WarehouseService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/warehouses")
public class WarehouseController {

    private final WarehouseService warehouseService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    public WarehouseController(WarehouseService warehouseService, ModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Get All Warehouses",
            response = Warehouse.class)
    public List<Warehouse> getWarehouses() {
        return warehouseService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get Warehouses By Id",
            response = Warehouse.class)
    public Warehouse getWarehouseById(@PathVariable int id) {
        try {
            return warehouseService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Create Warehouse",
            notes = "Only accessible by Employee",
            response = Warehouse.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Warehouse create(@RequestHeader HttpHeaders headers, @Valid @RequestBody WarehouseDto warehouseDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            Warehouse warehouse = new Warehouse();
            Address address = new Address();
            modelMapper.dtoToObject(warehouseDto.getAddress(), address);
            warehouse.setAddress(address);
            warehouseService.create(warehouse, address, user);
            return warehouse;
        } catch (UnexpectedRollbackException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Warehouse",
            notes = "Only accessible by Employee",
            response = Warehouse.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Warehouse update(@RequestHeader HttpHeaders headers,
                             @PathVariable int id,
                             @Valid @RequestBody WarehouseDto warehouseDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Warehouse warehouse = warehouseService.getById(id);
            Address address = modelMapper.fromDto(warehouseDto.getAddress(), warehouse.getAddress().getId());
            warehouseService.update(warehouse, address, user);
            return warehouse;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnexpectedRollbackException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Warehouse",
            notes = "Only accessible by Employee",
            response = Warehouse.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "ivan123",
            value = "See example below")
    private Warehouse delete(@RequestHeader HttpHeaders header, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Warehouse warehouseToBeDeleted = warehouseService.getById(id);
            warehouseService.delete(id, user);
            return warehouseToBeDeleted;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnexpectedRollbackException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
