package com.example.demo.controllers.mvc;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.Customer;
import com.example.demo.models.Employee;
import com.example.demo.models.User;
import com.example.demo.models.Warehouse;
import com.example.demo.services.contracts.CustomerService;
import com.example.demo.services.contracts.EmployeeService;
import com.example.demo.services.contracts.WarehouseService;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final CustomerService customerService;
    private final EmployeeService employeeService;
    private final WarehouseService warehouseService;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper,
                             CustomerService customerService,
                             EmployeeService employeeService,
                             WarehouseService warehouseService) {
        this.authenticationHelper = authenticationHelper;
        this.customerService = customerService;
        this.warehouseService = warehouseService;
        this.employeeService = employeeService;
    }

    @ModelAttribute("allCustomers")
    public List<Customer> getCustomerCount() {
        return customerService.filterCustomers(Optional.empty(),Optional.empty(),Optional.empty());
    }

    @ModelAttribute("allEmployees")
    public List<Employee> getEmployeeCount() {
        return employeeService.filterEmployees(Optional.empty(),Optional.empty(),Optional.empty());
    }

    @ModelAttribute("allWarehouses")
    public List<Warehouse> getWarehouseCount() {
        return warehouseService.getAll();
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException ignored) {
        }

        return "index";
    }
}
