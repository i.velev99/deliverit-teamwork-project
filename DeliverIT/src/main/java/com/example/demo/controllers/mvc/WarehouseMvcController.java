package com.example.demo.controllers.mvc;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.DuplicateEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.EditUserDto;
import com.example.demo.models.dto.EditWarehouseDto;
import com.example.demo.models.dto.RegisterCustomerDto;
import com.example.demo.models.dto.WarehouseDto;
import com.example.demo.services.contracts.CityService;
import com.example.demo.services.contracts.CountryService;
import com.example.demo.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/warehouses")
public class WarehouseMvcController {

    private final WarehouseService service;
    private final CityService cityService;
    private final CountryService countryService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public WarehouseMvcController(WarehouseService service,
                                  CityService cityService,
                                  CountryService countryService,
                                  AuthenticationHelper authenticationHelper,
                                  ModelMapper modelMapper) {
        this.service = service;
        this.cityService = cityService;
        this.countryService = countryService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("cities")
    public List<City> allCities() {
        return cityService.getAll();
    }

    @ModelAttribute("countries")
    public List<Country> allCountries() {
        return countryService.getAll();
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> warehouses() {
        return service.getAll();
    }
    @GetMapping
    public String showWarehouses(Model model, HttpSession session) {
        List<Warehouse> warehouses = service.getAll();
        model.addAttribute("warehouses", warehouses);

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
        }

        return "warehouses";
    }

    @GetMapping("/{id}")
    public String showSingleWarehouse(@PathVariable int id, Model model, HttpSession session) {
        try {
            Warehouse warehouse = service.getById(id);
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("warehouse", warehouse);
            return "warehouse";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/create")
    public String showCreateWarehousePage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session,"Employee");
            model.addAttribute("createWarehouseDto", new WarehouseDto());
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "create-warehouse";
    }


    @PostMapping("/create")
    public String handleCreateWarehouse(@Valid @ModelAttribute("createWarehouseDto") WarehouseDto dto,
                                         BindingResult bindingResult,
                                         HttpSession session,
                                         Model model) {
        if(bindingResult.hasErrors()) {
            return "create-warehouse";
        }

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session,"Employee");
            Warehouse warehouse = modelMapper.fromDto(dto);
            service.create(warehouse, warehouse.getAddress(), currentUser);
            return "redirect:/warehouses";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

    @GetMapping("/{id}/edit")
    public String showEditWarehousePage(@PathVariable int id, HttpSession session, Model model) {

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Warehouse warehouse = service.getById(id);
            EditWarehouseDto warehouseDto = modelMapper.fromWarehouse(warehouse);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("editWarehouseDto", warehouseDto);
            return "edit-warehouse";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

    @PostMapping("/{id}/edit")
    public String handleEditWarehouse(Model model,
                                 HttpSession session,
                                 @PathVariable int id,
                                 @Valid @ModelAttribute("editWarehouseDto") EditWarehouseDto dto,
                                 BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "edit-warehouse";
        }

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Warehouse byId = service.getById(id);
            Warehouse warehouse = modelMapper.fromDtoWarehouse(dto,id);
            Address address = modelMapper.fromDto(dto, dto.getAddressId());
            service.update(warehouse,address,currentUser);
        } catch (UnsupportedOperationException | EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "redirect:/warehouses/{id}";
    }

    @PostMapping("/{id}/delete")
    public String handleDeleteWarehouse(Model model,
                                   HttpSession session,
                                   @PathVariable int id){

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Warehouse warehouse = service.getById(id);
            service.delete(id,currentUser);
            return "redirect:/warehouses";
        } catch (UnsupportedOperationException | EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

}
