package com.example.demo.controllers.mvc;

import com.example.demo.controllers.AuthenticationHelper;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.models.dto.EditParcelDto;
import com.example.demo.models.dto.ParcelDto;
import com.example.demo.repositories.contracts.CategoryRepository;
import com.example.demo.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.demo.controllers.mvc.AuthenticationMvcController.CUSTOMER_ROLE;

@Controller
@RequestMapping()
public class ParcelMvcController {

    private final ParcelService service;
    private final CityService cityService;
    private final WarehouseService warehouseService;
    private final ShipmentService shipmentService;
    private final CategoryRepository categoryRepository;
    private final CustomerService customerService;
    private final CountryService countryService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;

    @Autowired
    public ParcelMvcController(ParcelService service,
                               CityService cityService,
                               WarehouseService warehouseService,
                               ShipmentService shipmentService,
                               CategoryRepository categoryRepository,
                               CustomerService customerService,
                               CountryService countryService,
                               AuthenticationHelper authenticationHelper,
                               ModelMapper modelMapper) {
        this.service = service;
        this.cityService = cityService;
        this.warehouseService = warehouseService;
        this.shipmentService = shipmentService;
        this.categoryRepository = categoryRepository;
        this.customerService = customerService;
        this.countryService = countryService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> getAllWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("shipments")
    public List<Shipment> getAllShipments() {
        return shipmentService.filterShipments(Optional.empty(),Optional.empty());
    }


    @ModelAttribute("categories")
    public List<Category> getAllCategories() {
        return categoryRepository.getAll();
    }

    @ModelAttribute("customers")
    public List<Customer> getAllCustomers() {
        return customerService.filterCustomers(Optional.empty(),Optional.empty(),Optional.empty());
    }

    @ModelAttribute("cities")
    public List<City> getAllCities() {
        return cityService.getAll();
    }

    @ModelAttribute("countries")
    public List<Country> getAllCountries() {
        return countryService.getAll();
    }

    @ModelAttribute("parcels")
    public List<Parcel> getAll() {
        return service.filterParcels(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

    @GetMapping("/my-parcels")
    public String showMyParcels(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, CUSTOMER_ROLE);
            model.addAttribute("currentUser", currentUser);
            List<Parcel> parcels = service.filterParcels(Optional.empty(),
                    Optional.empty(),
                    Optional.of(currentUser.getId()),
                    Optional.empty(),
                    Optional.empty(),
                    Optional.empty());
            model.addAttribute("myParcels", parcels);
            return "my-parcels";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/parcels")
    public String showParcelsPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "parcels";
    }


    @GetMapping("/parcels/{id}")
    public String showSingleParcel(@PathVariable int id, Model model, HttpSession session) {
        try {
            Parcel parcel = service.getById(id);
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("parcel", parcel);
            return "parcel";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/parcels/create")
    public String showCreateParcelPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session,"Employee");
            List<Shipment> shipments = shipmentService.getShipmentsByStatus(1);
            model.addAttribute("shipments", shipments);
            model.addAttribute("createParcelDto", new ParcelDto());
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "create-parcel";
    }

    @PostMapping("/parcels/create")
    public String handleCreateParcel(@Valid @ModelAttribute("createParcelDto") ParcelDto dto,
                                        BindingResult bindingResult,
                                        HttpSession session,
                                        Model model) {
        if(bindingResult.hasErrors()) {
            return "error";
        }

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session,"Employee");
            Parcel parcel = modelMapper.fromDto(dto);
            service.create(parcel, currentUser);
            return "redirect:/parcels";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

    @GetMapping("parcels/{id}/edit")
    public String showEditParcelPage(@PathVariable int id, HttpSession session, Model model) {

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Parcel parcel = service.getById(id);
            List<Shipment> shipmentsPreparing = shipmentService.getShipmentsByStatus(1);
            List<Shipment> shipmentsCompleted = shipmentService.getShipmentsByStatus(3);
            shipmentsPreparing.addAll(shipmentsCompleted);
            EditParcelDto parcelDto = modelMapper.fromParcel(parcel);
            model.addAttribute("shipments", shipmentsPreparing);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("editParcelDto", parcelDto);
            return "edit-parcel";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

    @PostMapping("/{id}/edit")
    public String handleEditParcel(Model model,
                                      HttpSession session,
                                      @PathVariable int id,
                                      @Valid @ModelAttribute("editParcelDto") EditParcelDto dto,
                                      BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "edit-parcel";
        }

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Parcel getById = service.getById(id);
            Parcel parcel = modelMapper.fromDtoParcel(dto, id);
            service.update(parcel, currentUser);
        } catch (UnsupportedOperationException | EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "redirect:/parcel/{id}";
    }

    @PostMapping("/parcels/{id}/delete")
    public String handleDeleteParcel(Model model,
                                        HttpSession session,
                                        @PathVariable int id){

        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, "Employee");
            Parcel parcel = service.getById(id);
            service.delete(id,currentUser);
            return "redirect:/warehouses";
        } catch (UnsupportedOperationException | EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }
}
