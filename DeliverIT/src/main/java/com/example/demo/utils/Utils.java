package com.example.demo.utils;

import com.example.demo.exceptions.DuplicateEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.Shipment;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.UserRepository;

import static com.example.demo.constants.Constants.*;

public class Utils {

    public static void checkDuplicateUsername(User user, UserRepository userRepository) {
        boolean duplicateExists = false;

        try {
            User result = userRepository.getByUsername(user.getUsername());
            if(user.getId() != result.getId()) {
                duplicateExists = true;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(USER_USERNAME_ERROR_MESSAGE);
        }
    }

    public static void checkDuplicateEmail(User user, UserRepository userRepository) {
        boolean duplicateExists = false;

        try {
            User result = userRepository.getByEmail(user.getEmail());
            if(user.getId() != result.getId()) {
                duplicateExists = true;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(USER_EMAIL_ERROR_MESSAGE);
        }
    }

    public static void checkShipmentOnTheWayStatus(Shipment shipment) {
        if (shipment.getStatus().getType().equals(ON_THE_WAY_STATUS)) {
            throw new UnsupportedOperationException(SHIPMENT_ON_THE_WAY_ERROR_MESSAGE);
        }
    }

    public static void checkShipmentPreparingStatus(Shipment shipment, String message) {
        if (shipment.getStatus().getId() != STATUS_PREPARING) {
            throw new UnsupportedOperationException(message);
        }
    }

    public static void checkEmployee(User user, String message) {
        if (user.getRole().getId() != EMPLOYEE_ROLE) {
            throw new UnauthorizedOperationException(message);
        }
    }

    public static void checkCustomer(User user, String message) {
        if (user.getRole().getId() != CUSTOMER_ROLE) {
            throw new UnauthorizedOperationException(message);
        }
    }


}
