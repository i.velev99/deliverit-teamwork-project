package com.example.demo.services;

import com.example.demo.models.City;
import com.example.demo.repositories.contracts.CityRepository;
import com.example.demo.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public List<City> getAll() {
        return new ArrayList<>(cityRepository.getAll());
    }

    @Override
    public City getById(int id) {
        return cityRepository.getById(id);
    }


}
