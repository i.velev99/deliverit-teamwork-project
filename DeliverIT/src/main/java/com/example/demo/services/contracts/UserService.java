package com.example.demo.services.contracts;

import com.example.demo.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);
}
