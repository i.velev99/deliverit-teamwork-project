package com.example.demo.services.contracts;

import com.example.demo.models.Employee;
import com.example.demo.models.Parcel;
import com.example.demo.models.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ParcelService {

    Parcel getById(int id);

    void create(Parcel parcel, User user);

    void update(Parcel parcel, User user);

    void delete(int id, User user);

    List<Parcel> filterParcels(Optional<Double> minWeight, Optional<Double> maxWeight,
                               Optional<Integer> customerId, Optional<Integer> warehouseId,
                               Optional<Integer> categoryId,Optional<String> sortParam);

}

