package com.example.demo.services;


import com.example.demo.models.Country;
import com.example.demo.repositories.contracts.CountryRepository;
import com.example.demo.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;


    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAll() {
        return new ArrayList<>(countryRepository.getAll());
    }

    @Override
    public Country getById(int id) {
        return countryRepository.getById(id);
    }

    public Country getByName(String name) {
        return countryRepository.getByName(name);
    }
}
