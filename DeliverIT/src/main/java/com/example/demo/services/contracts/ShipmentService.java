package com.example.demo.services.contracts;

import com.example.demo.models.*;
import org.springframework.http.HttpHeaders;

import java.util.List;
import java.util.Optional;

public interface ShipmentService {

    Shipment getShipmentById(int id);

    void create(Shipment shipment, User user);

    void update(Shipment shipment, User user);

    void delete(int id, User user);

    List<Shipment> getOnTheWayShipments(User user);

    List<Shipment> filterShipments(Optional<Integer> warehouseId, Optional<Integer> customerId);

    Shipment nextShipmentToArrive(User user);

    List<Shipment> shipmentsBelongingToCustomer(User user);

    List<Shipment> getShipmentsByStatus(int statusId);
}
