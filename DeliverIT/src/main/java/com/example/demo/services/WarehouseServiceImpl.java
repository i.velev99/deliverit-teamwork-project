package com.example.demo.services;

import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.Address;
import com.example.demo.models.Employee;
import com.example.demo.models.User;
import com.example.demo.models.Warehouse;
import com.example.demo.repositories.contracts.ParcelRepository;
import com.example.demo.repositories.contracts.WarehouseRepository;
import com.example.demo.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.demo.constants.Constants.WAREHOUSE_PARCELS_ATTACHED_ERROR_MESSAGE;
import static com.example.demo.utils.Utils.checkEmployee;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    public static final String WAREHOUSE_EMPLOYEE_ERROR_MESSAGE = "Only employee can modify warehouses.";
    private final WarehouseRepository warehouseRepository;
    private final ParcelRepository parcelRepository;

    @Autowired
    public WarehouseServiceImpl(WarehouseRepository warehouseRepository, ParcelRepository parcelRepository) {
        this.warehouseRepository = warehouseRepository;
        this.parcelRepository = parcelRepository;
    }

    @Override
    public List<Warehouse> getAll() {
        return warehouseRepository.getAll();
    }

    @Override
    public Warehouse getById(int id) {
        return warehouseRepository.getById(id);
    }

    @Override
    public void create(Warehouse warehouse, Address address, User user) {
        checkEmployee(user, WAREHOUSE_EMPLOYEE_ERROR_MESSAGE);
        warehouseRepository.create(warehouse, address);
    }

    @Override
    public void update(Warehouse warehouse, Address address, User user) {
        checkEmployee(user,WAREHOUSE_EMPLOYEE_ERROR_MESSAGE);
        warehouseRepository.update(warehouse,address);
    }

    @Override
    public void delete(int id, User user) {
        checkEmployee(user,WAREHOUSE_EMPLOYEE_ERROR_MESSAGE);
        if(!isParcelsEmpty(id, parcelRepository)) {
            throw new UnsupportedOperationException(WAREHOUSE_PARCELS_ATTACHED_ERROR_MESSAGE);
        }

        warehouseRepository.delete(id);
    }

    private boolean isParcelsEmpty(int id, ParcelRepository parcelRepository) {
        return parcelRepository.filterParcels(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.of(id),
                Optional.empty(),
                Optional.empty()).isEmpty();
    }

}
