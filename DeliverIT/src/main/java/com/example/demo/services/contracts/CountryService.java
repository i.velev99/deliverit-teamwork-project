package com.example.demo.services.contracts;

import com.example.demo.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();

    Country getById(int id);

    Country getByName(String name);
}
