package com.example.demo.services.contracts;

import com.example.demo.models.Employee;
import com.example.demo.models.User;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    List<Employee> filterEmployees(Optional<String> firstName, Optional<String> lastName, Optional<String> email);

    Employee getById(int id);

    void create(Employee employee);

    void update(Employee employee, User creator);

    void delete(int id, User creator);

    Employee getByUsername(String username);
}
