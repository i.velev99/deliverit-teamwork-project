package com.example.demo.services;

import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.Parcel;
import com.example.demo.models.Shipment;
import com.example.demo.models.Status;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.*;
import com.example.demo.services.contracts.ParcelService;
import com.example.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.example.demo.constants.Constants.*;
import static com.example.demo.constants.Constants.EMPLOYEE_PARCELS_ERROR_MESSAGE;
import static com.example.demo.utils.Utils.*;

@Service
public class ParcelServiceImpl implements ParcelService {

    private final ParcelRepository repository;
    private final ShipmentRepository shipmentRepository;


    @Autowired
    public ParcelServiceImpl(ParcelRepository repository, ShipmentRepository shipmentRepository) {
        this.repository = repository;
        this.shipmentRepository = shipmentRepository;
    }

    @Override
    public List<Parcel> filterParcels(Optional<Double> minWeight,
                                      Optional<Double> maxWeight,
                                      Optional<Integer> customerId,
                                      Optional<Integer> warehouseId,
                                      Optional<Integer> categoryId,
                                      Optional<String> sortParam) {
        return repository.filterParcels(minWeight, maxWeight, customerId, warehouseId, categoryId, sortParam);
    }

    @Override
    public Parcel getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Parcel parcel, User user) {

        checkEmployee(user, EMPLOYEE_PARCELS_ERROR_MESSAGE);

        Status status = shipmentRepository.getById(parcel.getShipment().getId()).getStatus();

        if (status.getId() != STATUS_PREPARING) {
            throw new UnsupportedOperationException(PARCEL_CREATE_ERROR_MESSAGE);
        }

        repository.create(parcel);
    }

    @Override
    public void update(Parcel parcel, User user) {

        checkEmployee(user, EMPLOYEE_PARCELS_ERROR_MESSAGE);

        Shipment shipment = repository.getById(parcel.getId()).getShipment();

        checkShipmentOnTheWayStatus(shipment);

        repository.update(parcel);
    }

    @Override
    public void delete(int id, User user) {

        checkEmployee(user, EMPLOYEE_PARCELS_ERROR_MESSAGE);

        Shipment shipment = repository.getById(id).getShipment();

        checkShipmentOnTheWayStatus(shipment);

        repository.delete(id);
    }


}
