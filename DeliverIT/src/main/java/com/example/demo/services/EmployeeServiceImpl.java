package com.example.demo.services;

import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.Employee;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.CustomerRepository;
import com.example.demo.repositories.contracts.EmployeeRepository;
import com.example.demo.repositories.contracts.UserRepository;
import com.example.demo.services.contracts.EmployeeService;
import com.example.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.demo.constants.Constants.*;


@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final UserRepository userRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, UserRepository userRepository) {
        this.employeeRepository = employeeRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Employee> filterEmployees(Optional<String> firstName, Optional<String> lastName, Optional<String> email) {

        return employeeRepository.filterEmployees(firstName, lastName, email);
    }

    @Override
    public Employee getById(int id) {
        return employeeRepository.getById(id);
    }

    @Override
    public void create(Employee employee) {

        Utils.checkDuplicateUsername(employee, userRepository);
        Utils.checkDuplicateEmail(employee, userRepository);

        employeeRepository.create(employee);
    }

    @Override
    public void update(Employee employee, User creator) {
        Utils.checkDuplicateUsername(employee, userRepository);
        Utils.checkDuplicateEmail(employee, userRepository);
        if (employee.getId() != creator.getId()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_CREATOR_ERROR_MESSAGE);
        }

        employeeRepository.update(employee);
    }

    @Override
    public void delete(int id, User creator) {

        if (!getById(id).getUsername().equals(creator.getUsername())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_CREATOR_ERROR_MESSAGE);
        }
        employeeRepository.delete(id);
    }

    @Override
    public Employee getByUsername(String username) {
        return employeeRepository.getByUsername(username);
    }


}
