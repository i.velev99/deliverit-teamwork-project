package com.example.demo.services.contracts;

import com.example.demo.models.Address;
import com.example.demo.models.Employee;
import com.example.demo.models.User;
import com.example.demo.models.Warehouse;

import java.util.List;

public interface WarehouseService {
    List<Warehouse> getAll();

    Warehouse getById(int id);

    void create(Warehouse warehouse, Address address, User user);

    void update(Warehouse warehouse, Address address, User user);

    void delete(int id, User user);
}
