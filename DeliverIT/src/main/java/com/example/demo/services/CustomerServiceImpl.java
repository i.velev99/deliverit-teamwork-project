package com.example.demo.services;

import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.exceptions.UserEntityException;
import com.example.demo.models.*;
import com.example.demo.repositories.contracts.CustomerRepository;
import com.example.demo.repositories.contracts.EmployeeRepository;
import com.example.demo.repositories.contracts.ParcelRepository;
import com.example.demo.repositories.contracts.UserRepository;
import com.example.demo.services.contracts.CustomerService;
import com.example.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


import static com.example.demo.constants.Constants.*;


@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final ParcelRepository parcelRepository;
    private final UserRepository userRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, ParcelRepository parcelRepository, UserRepository userRepository) {
        this.customerRepository = customerRepository;
        this.parcelRepository = parcelRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Customer> filterCustomers(Optional<String> firstName, Optional<String> lastName, Optional<String> email) {

        return customerRepository.filterCustomers(firstName, lastName, email);
    }

    @Override
    public Customer getById(int id) {
        return customerRepository.getById(id);
    }

    @Override
    public void create(Customer customer, Address address) {
        Utils.checkDuplicateUsername(customer, userRepository);
        Utils.checkDuplicateEmail(customer, userRepository);
        customerRepository.create(customer, address);
    }

    @Override
    public void update(Customer customer, Address address, User creator) {
        Utils.checkDuplicateUsername(customer, userRepository);
        Utils.checkDuplicateEmail(customer, userRepository);

        if (creator.getId() != customer.getId()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_CREATOR_ERROR_MESSAGE);
        }

        customerRepository.update(customer, address);
    }

    @Override
    public void delete(int id, User creator) {

        if (!getById(id).getCreator().getUsername().equals(creator.getUsername())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_CREATOR_ERROR_MESSAGE);
        }


        if (!isParcelsEmpty(id, parcelRepository)) {
            throw new UserEntityException(CUSTOMER_ATTACHED_ERROR_MESSAGE);
        }

        customerRepository.delete(id);
    }

    @Override
    public Customer getByUsername(String username) {
        return customerRepository.getByUsername(username);
    }

    @Override
    public List<Parcel> incomingParcels(User user) {
        return customerRepository.ownedParcelsByStatus(user, STATUS_ON_THE_WAY);
    }

    @Override
    public List<Parcel> pastParcels(User user) {
        return customerRepository.ownedParcelsByStatus(user, STATUS_COMPLETED);
    }

    boolean isParcelsEmpty(int id, ParcelRepository parcelRepository) {
        return parcelRepository.filterParcels(Optional.empty(),
                Optional.empty(),
                Optional.of(id),
                Optional.empty(),
                Optional.empty(),
                Optional.empty()).isEmpty();
    }


}
