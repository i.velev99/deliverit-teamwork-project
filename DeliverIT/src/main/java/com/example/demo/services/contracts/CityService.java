package com.example.demo.services.contracts;

import com.example.demo.models.City;

import java.util.List;

public interface CityService {
    List<City> getAll();

    City getById(int id);


}
