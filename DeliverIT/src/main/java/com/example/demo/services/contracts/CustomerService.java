package com.example.demo.services.contracts;

import com.example.demo.models.Address;
import com.example.demo.models.Customer;
import com.example.demo.models.Parcel;
import com.example.demo.models.User;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<Customer> filterCustomers(Optional<String> firstName, Optional<String> lastName, Optional<String> email);

    Customer getById(int id);

    void create(Customer customer, Address address);

    void update(Customer customer, Address address, User creator);

    void delete(int id, User creator);

    Customer getByUsername(String username);

    List<Parcel> incomingParcels(User User);

    List<Parcel> pastParcels(User user);

}
