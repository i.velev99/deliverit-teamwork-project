package com.example.demo.services;

import com.example.demo.models.*;
import com.example.demo.repositories.contracts.ShipmentRepository;
import com.example.demo.services.contracts.ShipmentService;
import com.example.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.example.demo.constants.Constants.*;
import static com.example.demo.utils.Utils.*;

import java.util.List;
import java.util.Optional;

@Service
public class ShipmentServiceImpl implements ShipmentService {

    private final ShipmentRepository repository;

    @Autowired
    public ShipmentServiceImpl(ShipmentRepository repository) {
        this.repository = repository;

    }

    @Override
    public List<Shipment> filterShipments(Optional<Integer> warehouseId, Optional<Integer> customerId) {

        return repository.filterShipments(warehouseId, customerId);
    }

    @Override
    public List<Shipment> getOnTheWayShipments(User user) {

        checkCustomer(user, CUSTOMER_SHIPMENTS_ERROR_MESSAGE);
        return repository.getShipmentsByStatus(STATUS_ON_THE_WAY);
    }

    @Override
    public Shipment nextShipmentToArrive(User user) {
        checkEmployee(user, NEXT_SHIPMENT_ERROR_MESSAGE);
        return repository.nextShipmentToArrive();
    }

    @Override
    public List<Shipment> shipmentsBelongingToCustomer(User user) {

        checkCustomer(user, CUSTOMER_SHIPMENTS_ERROR_MESSAGE);

        return repository.shipmentsBelongingToCustomer(user);
    }

    @Override
    public List<Shipment> getShipmentsByStatus(int statusId) {
        return repository.getShipmentsByStatus(statusId);
    }

    @Override
    public Shipment getShipmentById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Shipment shipment, User user) {

        checkEmployee(user, SHIPMENTS_EMPLOYEES_ERROR_MESSAGE);

        checkShipmentPreparingStatus(shipment, SHIPMENT_STATUS_ERROR_MESSAGE);

        repository.create(shipment);
    }

    @Override
    public void update(Shipment shipment, User user) {

        checkEmployee(user, SHIPMENTS_EMPLOYEES_ERROR_MESSAGE);

        repository.update(shipment);
    }

    @Override
    public void delete(int id, User user) {

        checkEmployee(user, SHIPMENTS_EMPLOYEES_ERROR_MESSAGE);

        Shipment shipmentById = getShipmentById(id);

        Utils.checkShipmentOnTheWayStatus(shipmentById);

        if (!shipmentById.getParcels().isEmpty()) {
            throw new UnsupportedOperationException(SHIPMENT_PARCELS_ATTACHED_ERROR_MESSAGE);
        }

        repository.delete(id);
    }
}
