package com.example.demo.exceptions;

public class UserEntityException extends RuntimeException{
    public UserEntityException(String message) {
        super(message);
    }
}
