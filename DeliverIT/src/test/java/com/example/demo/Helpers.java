package com.example.demo;

import com.example.demo.models.*;

import java.util.Date;

public class Helpers {

    public static Customer createMockCustomer() {
        return new Customer(2 ,"Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf",
                createMockRole(), createMockAddress());
    }

    public static Employee createMockEmployee() {
        return new Employee(1, "Ivan", "Velev", "i.velev999@gmail.com", "ivan1","asdf",
                new Role(1, "Employee"));
    }

    public static Address createMockAddress() {
        return new Address(1, "ul. Ivan Vazov 2",
                createMockCity());
    }

    public static City createMockCity() {
        return new City(1, "Sofia", createMockCountry());
    }

    public static Country createMockCountry() {
        Country country = new Country();
        country.setId(1);
        country.setName("Bulgaria");
        return country;
    }

    public static Role createMockRole() {
        Role role = new Role();
        role.setId(2);
        role.setName("Customer");
        return role;
    }

    public static Shipment createMockShipment() {
        return new Shipment(1,
                new Date(2021),
                new Date(2021),
                createMockStatus());
    }

    public static Status createMockStatus() {
        Status status = new Status();
        status.setId(1);
        status.setType("Preparing");
        return status;
    }

    public static Category createMockCategory() {
        return new Category(1,"Medical");
    }

    public static Warehouse createMockWarehouse() {
        return new Warehouse(1, createMockAddress());
    }

    public static Parcel createMockParcel() {
        Parcel parcel = new Parcel();
        parcel.setId(1);
        parcel.setWeight(1.1);
        parcel.setCategory(createMockCategory());
        parcel.setCustomer(createMockCustomer());
        parcel.setWarehouse(createMockWarehouse());
        return parcel;
    }

}
