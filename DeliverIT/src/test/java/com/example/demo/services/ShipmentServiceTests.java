package com.example.demo.services;

import com.example.demo.exceptions.EmptyEntityException;
import com.example.demo.models.Shipment;
import com.example.demo.models.Status;
import com.example.demo.repositories.contracts.ShipmentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.example.demo.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ShipmentServiceTests {
    @Mock
    ShipmentRepository repository;


    @InjectMocks
    ShipmentServiceImpl service;

    @Test
    public void getById_Should_ReturnCountry_When_MatchExists() {
        //Arrange
        var mockShipment = createMockShipment();
        var mockStatus = createMockStatus();
        Mockito.when(repository.getById(mockShipment.getId()))
                .thenReturn(mockShipment);

        //Act
        Shipment result = service.getShipmentById(1);

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals(mockStatus, mockShipment.getStatus());
    }

    @Test
    public void getOnTheWayShipments_Should_ReturnShipments_When_ThereAreAny() {
        //Arrange
        var mockShipment = createMockShipment();
        mockShipment.setStatus(new Status(2, "onTheWay"));

        List<Shipment> shipments = new ArrayList<>();
        shipments.add(mockShipment);
        Mockito.when(repository.getShipmentsByStatus(2))
                .thenReturn(shipments);

        //Act
        List<Shipment> result = service.getOnTheWayShipments(createMockCustomer());

        //Assert
        Assertions.assertEquals(1, result.get(0).getId());
        Assertions.assertEquals(mockShipment.getStatus()
                , result.get(0).getStatus());
    }

    @Test
    public void nextShipmentToArrive_Should_CallInRepository() {
        //Arrange
        var mockShipment = createMockShipment();

        Mockito.when(repository.nextShipmentToArrive())
                .thenReturn(mockShipment);

        //Act
        Shipment result = service.nextShipmentToArrive(createMockEmployee());

        //Assert
        Assertions.assertEquals(result, mockShipment);
    }

    @Test
    public void shipmentsBelongingToCustomer_Should_CallInRepository() {
        //Arrange
        var mockShipment = createMockShipment();

        //Act
        List<Shipment> result = service.shipmentsBelongingToCustomer(createMockCustomer());

        //Assert
        Assertions.assertEquals(result, new ArrayList<>());
    }

    @Test
    public void delete_Should_ThrowException_When_WhenShipmentIsOnTheWay() {
        //Arrange
        var mockShipment = createMockShipment();
        mockShipment.setStatus(new Status(2, "On the way"));

        //Act
        Mockito.when(service.getShipmentById(mockShipment.getId()))
                .thenReturn(mockShipment);

        //Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> service.delete(mockShipment.getId(),createMockEmployee()));
    }

    @Test
    public void delete_Should_ThrowException_When_ShipmentHasParcels() {
        //Arrange
        var mockShipment = createMockShipment();
        var mockParcel = createMockParcel();
        mockShipment.setParcels(Set.of(mockParcel));

        //Act
        Mockito.when(service.getShipmentById(mockShipment.getId()))
                .thenReturn(mockShipment);

        //Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> service.delete(mockShipment.getId(),createMockEmployee()));
    }

    @Test
    public void delete_Should_CallInRepository_When_ShipmentIdIsCorrect() {
        //Arrange
        var mockShipment = createMockShipment();

        //Act
        Mockito.when(service.getShipmentById(mockShipment.getId()))
                .thenReturn(mockShipment);
        service.delete(mockShipment.getId(),createMockEmployee());

        //Assert
        Mockito.verify(repository, Mockito.times(1))
                .delete(1);
    }

    @Test
    public void update_Should_CallInRepository() {
        //Arrange
        var mockShipment = createMockShipment();

        //Act
        service.update(mockShipment,createMockEmployee());

        //Assert
        Mockito.verify(repository, Mockito.times(1))
                .update(Mockito.any(Shipment.class));
    }

    @Test
    public void create_Should_CallInRepository() {
        //Arrange
        var mockShipment = createMockShipment();

        //Act
        service.create(mockShipment,createMockEmployee());

        //Assert
        Mockito.verify(repository, Mockito.times(1))
                .create(Mockito.any(Shipment.class));
    }
    @Test
    public void create_Should_ThrowException_When_WhenShipmentIsDifferentFromPreparing() {
        //Arrange
        var mockShipment = createMockShipment();
        mockShipment.setStatus(new Status(2, "On the way"));

        //Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> service.create(mockShipment,createMockEmployee()));
    }
}
