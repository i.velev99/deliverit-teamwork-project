package com.example.demo.services;

import com.example.demo.exceptions.DuplicateEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.exceptions.UserEntityException;
import com.example.demo.models.*;
import com.example.demo.repositories.contracts.CustomerRepository;
import com.example.demo.repositories.contracts.EmployeeRepository;
import com.example.demo.repositories.contracts.ParcelRepository;
import com.example.demo.repositories.contracts.UserRepository;
import com.example.demo.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.*;

import static com.example.demo.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTests {

    @Mock
    CustomerRepository customerRepository;
    @Mock
    ParcelRepository parcelRepository;
    @Mock
    EmployeeRepository employeeRepository;
    @Mock
    UserRepository userRepository;

    @InjectMocks
    CustomerServiceImpl customerService;

    @Test
    public void getById_Should_ReturnCustomer_When_MatchExists() {

        //Arrange
        Mockito.when(customerRepository.getById(2))
                .thenReturn(new Customer(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf",
                        new Role(2, "Customer"),
                        new Address(1, "ul. Ivan Vazov 2",
                                new City(1, "Sofia", new Country(1, "Bulgaria")))));

        //Act
        Customer result = customerService.getById(2);

        //Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("Ivan", result.getFirstName());
        Assertions.assertEquals("Velev", result.getLastName());
        Assertions.assertEquals("i.velev99@gmail.com", result.getEmail());
        Assertions.assertEquals("ivan", result.getUsername());
        Assertions.assertEquals("asdf", result.getPassword());
        Assertions.assertEquals(new Role(2, "Customer").getId(), result.getRole().getId());
        Assertions.assertEquals(new Address(1, "ul. Ivan Vazov 2",
                new City(1, "Sofia", new Country(1, "Bulgaria"))).getId(), result.getAddress().getId());

    }

    @Test
    public void getByUsername_Should_ReturnCustomer_When_MatchExists() {

        //Arrange
        Mockito.when(customerRepository.getByUsername("ivan"))
                .thenReturn(new Customer(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf",
                        new Role(2, "Customer"),
                        new Address(1, "ul. Ivan Vazov 2",
                                new City(1, "Sofia", new Country(1, "Bulgaria")))));

        //Act
        Customer result = customerService.getByUsername("ivan");

        //Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("Ivan", result.getFirstName());
        Assertions.assertEquals("Velev", result.getLastName());
        Assertions.assertEquals("i.velev99@gmail.com", result.getEmail());
        Assertions.assertEquals("ivan", result.getUsername());
        Assertions.assertEquals("asdf", result.getPassword());
        Assertions.assertEquals(new Role(2, "Customer").getId(), result.getRole().getId());
        Assertions.assertEquals(new Address(1, "ul. Ivan Vazov 2",
                new City(1, "Sofia", new Country(1, "Bulgaria"))).getId(), result.getAddress().getId());

//        //Arrange
//
//        Customer customer = new Customer(2 ,"Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf",
//                new Role(2, "Customer"),
//                new Address(1, "ul. Ivan Vazov 2",
//                        new City(1, "Sofia", new Country(1, "Bulgaria"))));
//
//        Mockito.when(customerRepository.getById(2))
//                .thenReturn(customer);
//
//        //Act
//        Customer result = customerService.getById(2);
//
//        //Assert
//        Assertions.assertEquals(customer.getId(), result.getId());
//        Assertions.assertEquals(customer.getFirstName(), result.getFirstName());
//        Assertions.assertEquals(customer.getLastName(), result.getLastName());
//        Assertions.assertEquals(customer.getEmail(), result.getEmail());
//        Assertions.assertEquals(customer.getUsername(), result.getUsername());
//        Assertions.assertEquals(customer.getPassword(), result.getPassword());
//        Assertions.assertEquals(customer.getRole(), result.getRole());
//        Assertions.assertEquals(customer.getAddress(), result.getAddress());
    }

    @Test
    public void create_Should_Throw_When_CustomerWithSameUsernameExists() {
        //Arrange
        Customer customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenReturn(customer);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.create(customer, address));
    }

    @Test
    public void create_Should_Throw_When_EmployeeWithSameUsernameExists() {
        //Arrange
        Customer customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", new Role(1, "Employee")));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.create(customer, address));
    }

    @Test
    public void create_Should_Throw_When_CustomerWithSameEmailExists() {
        //Arrange
        Customer customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(customer.getEmail()))
                .thenReturn(customer);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.create(customer, address));
    }

    @Test
    public void create_Should_Throw_When_EmployeeWithSameEmailExists() {
        //Arrange
        Customer customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(customer.getEmail()))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", new Role(1, "Employee")));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.create(customer, address));
    }

    @Test
    public void create_Should_CallRepository_when_UserCredentialsUnique() {

        var customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(customer.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        customerService.create(customer, address);

        Mockito.verify(customerRepository, Mockito.times(1))
                .create(customer, address);
    }


    @Test
    public void update_Should_Throw_When_CustomerWithSameUsernameExists() {
        //Arrange
        Customer customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenReturn(customer);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.update(customer, address, customer));
    }

    @Test
    public void update_Should_Throw_When_CustomerWithSameEmailExists() {
        //Arrange
        Customer customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(customer.getEmail()))
                .thenReturn(customer);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.create(customer, address));
    }

    @Test
    public void update_Should_Throw_When_EmployeeWithSameUsernameExists() {
        //Arrange
        Customer customer = createMockCustomer();
        Address address = createMockAddress();
        customer.setCreator(customer);

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", new Role(1, "Employee")));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.update(customer, address, customer));
    }

    @Test
    public void update_Should_Throw_When_EmployeeWithSameEmailExists() {
        //Arrange
        Customer customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(customer.getEmail()))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", new Role(1, "Employee")));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.update(customer, address, customer));
    }

    @Test
    public void update_Should_CallRepository_when_UserCredentialsUnique() {

        var customer = createMockCustomer();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(customer.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        customerService.update(customer, address, customer);

        Mockito.verify(customerRepository, Mockito.times(1))
                .update(customer, address);
    }

    @Test
    public void update_Should_Throw_When_UserIsNotCreator() {
        var customer = createMockCustomer();
        Address address = createMockAddress();

        var testCreator = new Customer(4, "asdf", "asdf", "asdfg@gmail.com", "asdfg", "asdf", createMockRole(), address);

        Mockito.when(userRepository.getByUsername(customer.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(customer.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> customerService.update(customer, address, testCreator));

    }

    @Test
    public void delete_Should_Throw_When_UserIsNotCreator() {
        var customer = createMockCustomer();
        Address address = createMockAddress();
        var testCustomer = new Customer(1, "asdf", "asdf", "asdf@gmail.com", "asdf", "asdf", createMockRole(), address);
        customer.setCreator(customer);

        Mockito.when(customerService.getById(customer.getId()))
                .thenReturn(customer);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> customerService.delete(customer.getId(), testCustomer));
    }

    @Test
    public void delete_Should_Throw_When_CustomerHasParcelAttached() {
        var customer = createMockCustomer();
        customer.setCreator(customer);
        var parcel = createMockParcel();

        List<Parcel> parcels = new ArrayList<>();
        parcels.add(parcel);

        Mockito.when(customerService.getById(customer.getId()))
                .thenReturn(customer);

        Mockito.when(parcelRepository.filterParcels(Optional.empty(), Optional.empty(),Optional.of(customer.getId()), Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>(parcels));


        Assertions.assertThrows(UserEntityException.class,
                () -> customerService.delete(customer.getId(), customer));
    }

    @Test
    public void delete_Should_CallRepository_When_CustomerIsCreatorAndThereAreNoParcelsAttached() {
        var customer = createMockCustomer();
        customer.setCreator(customer);

        Mockito.when(customerService.getById(customer.getId()))
                .thenReturn(customer);

        Mockito.when(parcelRepository.filterParcels(Optional.empty(), Optional.empty(),Optional.of(customer.getId()),
                Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        customerService.delete(customer.getId(), customer);

        Mockito.verify(customerRepository, Mockito.times(1))
                .delete(customer.getId());
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_NoParameters() {
        Mockito.when(customerRepository.filterCustomers(Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());
        customerRepository.filterCustomers(Optional.empty(), Optional.empty(), Optional.empty());
        Mockito.verify(customerRepository, Mockito.times(1))
                .filterCustomers(Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_FirstNameParameterOnly() {
        var customer = createMockCustomer();

        List<Customer> customers = new ArrayList<>();
        customers.add(customer);

        customerRepository.filterCustomers(Optional.of(customer.getFirstName()), Optional.empty(), Optional.empty());

        Mockito.verify(customerRepository, Mockito.times(1))
                .filterCustomers(Optional.of(customer.getFirstName()), Optional.empty(), Optional.empty());
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_LastNameParameterOnly() {
        var customer = createMockCustomer();

        List<Customer> customers = new ArrayList<>();
        customers.add(customer);

        customerRepository.filterCustomers(Optional.empty(), Optional.of(customer.getLastName()), Optional.empty());

        Mockito.verify(customerRepository, Mockito.times(1))
                .filterCustomers(Optional.empty(), Optional.of(customer.getLastName()), Optional.empty());
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_EmailParameterOnly() {
        var customer = createMockCustomer();

        List<Customer> customers = new ArrayList<>();
        customers.add(customer);

        customerRepository.filterCustomers(Optional.empty(), Optional.empty(), Optional.of(customer.getEmail()));

        Mockito.verify(customerRepository, Mockito.times(1))
                .filterCustomers(Optional.empty(), Optional.empty(), Optional.of(customer.getEmail()));
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_AllParametersExists() {
        var customer = createMockCustomer();

        List<Customer> customers = new ArrayList<>();
        customers.add(customer);

        customerRepository.filterCustomers(Optional.of(customer.getFirstName()), Optional.of(customer.getLastName()), Optional.of(customer.getEmail()));

        Mockito.verify(customerRepository, Mockito.times(1))
                .filterCustomers(Optional.of(customer.getFirstName()), Optional.of(customer.getLastName()), Optional.of(customer.getEmail()));
    }

//    @Test
//    public void incomingParcels_Should_CallRepository() {
//        var customer = createMockCustomer();
//        var parcel = createMockParcel();
//        Status status = new Status(2, "On the way");
//        var shipment= createMockShipment();
//        shipment.setStatus(status);
//        List<Parcel> parcels = new ArrayList<>();
//        parcels.add(parcel);
//
//        Mockito.when(customerRepository.ownedParcelsByStatus(customer, status.getId()))
//                .thenReturn(parcels);
//
//        Mockito.verify(customerRepository, Mockito.times(1))
//                .ownedParcelsByStatus(customer, status.getId());
//    }
}
