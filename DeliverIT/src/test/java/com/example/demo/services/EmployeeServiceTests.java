package com.example.demo.services;

import com.example.demo.exceptions.DuplicateEntityException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.repositories.contracts.CustomerRepository;
import com.example.demo.repositories.contracts.EmployeeRepository;
import com.example.demo.repositories.contracts.ParcelRepository;
import com.example.demo.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.demo.Helpers.*;
import static com.example.demo.Helpers.createMockCustomer;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTests {

    @Mock
    CustomerRepository customerRepository;
    @Mock
    ParcelRepository parcelRepository;
    @Mock
    EmployeeRepository employeeRepository;
    @Mock
    UserRepository userRepository;

    @InjectMocks
    EmployeeServiceImpl employeeService;

    @Test
    public void getById_Should_ReturnCustomer_When_MatchExists() {

        //Arrange
        Mockito.when(employeeRepository.getById(2))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf",
                        new Role(2, "Customer")));

        //Act
        Employee result = employeeService.getById(2);

        //Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("Ivan", result.getFirstName());
        Assertions.assertEquals("Velev", result.getLastName());
        Assertions.assertEquals("i.velev99@gmail.com", result.getEmail());
        Assertions.assertEquals("ivan", result.getUsername());
        Assertions.assertEquals("asdf", result.getPassword());
        Assertions.assertEquals(new Role(2, "Customer"), result.getRole());

//        //Arrange
//
//        Customer customer = new Customer(2 ,"Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf",
//                new Role(2, "Customer"),
//                new Address(1, "ul. Ivan Vazov 2",
//                        new City(1, "Sofia", new Country(1, "Bulgaria"))));
//
//        Mockito.when(customerRepository.getById(2))
//                .thenReturn(customer);
//
//        //Act
//        Customer result = customerService.getById(2);
//
//        //Assert
//        Assertions.assertEquals(customer.getId(), result.getId());
//        Assertions.assertEquals(customer.getFirstName(), result.getFirstName());
//        Assertions.assertEquals(customer.getLastName(), result.getLastName());
//        Assertions.assertEquals(customer.getEmail(), result.getEmail());
//        Assertions.assertEquals(customer.getUsername(), result.getUsername());
//        Assertions.assertEquals(customer.getPassword(), result.getPassword());
//        Assertions.assertEquals(customer.getRole(), result.getRole());
//        Assertions.assertEquals(customer.getAddress(), result.getAddress());
    }

    @Test
    public void getByUsername_Should_ReturnCustomer_When_MatchExists() {

        //Arrange
        Mockito.when(employeeRepository.getByUsername("ivan"))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf",
                        new Role(2, "Customer")));

        //Act
        Employee result = employeeService.getByUsername("ivan");

        //Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("Ivan", result.getFirstName());
        Assertions.assertEquals("Velev", result.getLastName());
        Assertions.assertEquals("i.velev99@gmail.com", result.getEmail());
        Assertions.assertEquals("ivan", result.getUsername());
        Assertions.assertEquals("asdf", result.getPassword());
        Assertions.assertEquals(new Role(2, "Customer"), result.getRole());

//        //Arrange
//
//        Customer customer = new Customer(2 ,"Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf",
//                new Role(2, "Customer"),
//                new Address(1, "ul. Ivan Vazov 2",
//                        new City(1, "Sofia", new Country(1, "Bulgaria"))));
//
//        Mockito.when(customerRepository.getById(2))
//                .thenReturn(customer);
//
//        //Act
//        Customer result = customerService.getById(2);
//
//        //Assert
//        Assertions.assertEquals(customer.getId(), result.getId());
//        Assertions.assertEquals(customer.getFirstName(), result.getFirstName());
//        Assertions.assertEquals(customer.getLastName(), result.getLastName());
//        Assertions.assertEquals(customer.getEmail(), result.getEmail());
//        Assertions.assertEquals(customer.getUsername(), result.getUsername());
//        Assertions.assertEquals(customer.getPassword(), result.getPassword());
//        Assertions.assertEquals(customer.getRole(), result.getRole());
//        Assertions.assertEquals(customer.getAddress(), result.getAddress());
    }

    @Test
    public void create_Should_Throw_When_EmployeeWithSameUsernameExists() {
        //Arrange
        Employee employee = createMockEmployee();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenReturn(employee);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.create(employee));
    }

    @Test
    public void create_Should_Throw_When_CustomerWithSameUsernameExists() {
        //Arrange
        Employee employee = createMockEmployee();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenReturn(new Customer(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", createMockRole(),createMockAddress()));


        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.create(employee));
    }

    @Test
    public void create_Should_Throw_When_CustomerWithSameEmailExists() {
        //Arrange
        Employee employee = createMockEmployee();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(employee.getEmail()))
                .thenReturn(new Customer(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", createMockRole(),createMockAddress()));


        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.create(employee));
    }

    @Test
    public void create_Should_Throw_When_EmployeeWithSameEmailExists() {
        //Arrange
        Employee employee = createMockEmployee();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(employee.getEmail()))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", new Role(1, "Employee")));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.create(employee));
    }

    @Test
    public void create_Should_CallRepository_when_UserCredentialsUnique() {

        var employee = createMockEmployee();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(employee.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        employeeService.create(employee);

        Mockito.verify(employeeRepository, Mockito.times(1))
                .create(employee);
    }


    @Test
    public void update_Should_Throw_When_CustomerWithSameUsernameExists() {
        //Arrange
        Employee employee = createMockEmployee();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenReturn(new Customer(2, "Ivan", "Velev", "i.velev999@gmail.com", "ivan", "asdf", new Role(1, "Employee"), createMockAddress()));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.update(employee, employee));
    }

    @Test
    public void update_Should_Throw_When_CustomerWithSameEmailExists() {
        //Arrange
        Employee employee = createMockEmployee();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(employee.getEmail()))
                .thenReturn(new Customer(2, "Ivan", "Velev", "i.velev999@gmail.com", "ivan", "asdf", new Role(1, "Employee"), createMockAddress()));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.create(employee));
    }

    @Test
    public void update_Should_Throw_When_EmployeeWithSameUsernameExists() {
        //Arrange
        Employee employee = createMockEmployee();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", new Role(1, "Employee")));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.update(employee, employee));
    }

    @Test
    public void update_Should_Throw_When_EmployeeWithSameEmailExists() {
        //Arrange
        Employee employee = createMockEmployee();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(employee.getEmail()))
                .thenReturn(new Employee(2, "Ivan", "Velev", "i.velev99@gmail.com", "ivan", "asdf", new Role(1, "Employee")));

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.update(employee, employee));
    }

    @Test
    public void update_Should_CallRepository_when_UserCredentialsUnique() {

        var employee = createMockEmployee();
        Address address = createMockAddress();

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(employee.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        employeeService.update(employee, employee);

        Mockito.verify(employeeRepository, Mockito.times(1))
                .update(employee);
    }

    @Test
    public void update_Should_Throw_When_UserIsNotCreator() {
        var employee = createMockEmployee();

        var testCreator = new Employee(4, "asdf", "asdf", "asdfg@gmail.com", "asdfg", "asdf", createMockRole());

        Mockito.when(userRepository.getByUsername(employee.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(userRepository.getByEmail(employee.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.update(employee, testCreator));

    }

    @Test
    public void delete_Should_Throw_When_UserIsNotCreator() {
        var employee = createMockEmployee();
        var testCustomer = new Employee(1, "asdf", "asdf", "asdf@gmail.com", "asdf", "asdf", createMockRole());
        employee.setCreator(employee);

        Mockito.when(employeeService.getById(employee.getId()))
                .thenReturn(employee);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.delete(employee.getId(), testCustomer));
    }

    @Test
    public void delete_Should_CallRepository_When_EmployeeIsCreator() {
        var employee = createMockEmployee();
        var creator = createMockEmployee();
        //employee.setCreator(employee);

        Mockito.when(employeeService.getById(employee.getId()))
                .thenReturn(employee);

        employeeService.delete(employee.getId(), creator);

        Mockito.verify(employeeRepository, Mockito.times(1))
                .delete(employee.getId());
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_NoParameters() {
        Mockito.when(employeeRepository.filterEmployees(Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());
        employeeRepository.filterEmployees(Optional.empty(), Optional.empty(), Optional.empty());
        Mockito.verify(employeeRepository, Mockito.times(1))
                .filterEmployees(Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_FirstNameParameterOnly() {
        var employee = createMockEmployee();

        List<Employee> employees = new ArrayList<>();
        employees.add(employee);

        employeeRepository.filterEmployees(Optional.of(employee.getFirstName()), Optional.empty(), Optional.empty());

        Mockito.verify(employeeRepository, Mockito.times(1))
                .filterEmployees(Optional.of(employee.getFirstName()), Optional.empty(), Optional.empty());
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_LastNameParameterOnly() {
        var employee = createMockEmployee();

        List<Employee> employees = new ArrayList<>();
        employees.add(employee);

        employeeRepository.filterEmployees(Optional.empty(), Optional.of(employee.getLastName()), Optional.empty());

        Mockito.verify(employeeRepository, Mockito.times(1))
                .filterEmployees(Optional.empty(), Optional.of(employee.getLastName()), Optional.empty());
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_EmailParameterOnly() {
        var employee = createMockEmployee();

        List<Employee> employees = new ArrayList<>();
        employees.add(employee);

        employeeRepository.filterEmployees(Optional.empty(), Optional.empty(), Optional.of(employee.getEmail()));

        Mockito.verify(employeeRepository, Mockito.times(1))
                .filterEmployees(Optional.empty(), Optional.empty(), Optional.of(employee.getEmail()));
    }

    @Test
    public void filterCustomers_Should_CallRepository_When_AllParametersExists() {
        var employee = createMockEmployee();

        List<Employee> employees = new ArrayList<>();
        employees.add(employee);

        employeeRepository.filterEmployees(Optional.of(employee.getFirstName()), Optional.of(employee.getLastName()), Optional.of(employee.getEmail()));

        Mockito.verify(employeeRepository, Mockito.times(1))
                .filterEmployees(Optional.of(employee.getFirstName()), Optional.of(employee.getLastName()), Optional.of(employee.getEmail()));
    }
}
