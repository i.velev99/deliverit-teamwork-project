package com.example.demo.services;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.exceptions.UnauthorizedOperationException;
import com.example.demo.models.*;
import com.example.demo.repositories.contracts.ParcelRepository;
import com.example.demo.repositories.contracts.WarehouseRepository;
import com.example.demo.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.demo.Helpers.*;
import static com.example.demo.Helpers.createMockRole;

@ExtendWith(MockitoExtension.class)
public class WarehouseServiceTests {

    @Mock
    WarehouseRepository warehouseRepository;
    @Mock
    ParcelRepository parcelRepository;

    @InjectMocks
    WarehouseServiceImpl warehouseService;

    @Test
    public void getById_Should_ReturnWarehouse_When_MatchExists() {

        //Arrange
        Mockito.when(warehouseRepository.getById(2))
                .thenReturn(new Warehouse(2, createMockAddress()));

        //Act
        Warehouse result = warehouseService.getById(2);

        //Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals(new Address(1, "ul. Ivan Vazov 2",
                new City(1, "Sofia", new Country(1, "Bulgaria"))).getId(), result.getAddress().getId());

    }

    @Test
    public void create_Should_CallRepository() {
        var warehouse = createMockWarehouse();
        var address = createMockAddress();
        var employee = createMockEmployee();

        warehouseService.create(warehouse,address,employee);

        Mockito.verify(warehouseRepository, Mockito.times(1))
                .create(warehouse, address);
    }


    @Test
    public void update_Should_CallRepository_when_UserCredentialsUnique() {

        var warehouse = createMockWarehouse();
        var employee = createMockEmployee();
        Address address = createMockAddress();

        warehouseService.update(warehouse, address, employee);

        Mockito.verify(warehouseRepository, Mockito.times(1))
                .update(warehouse, address);
    }


    @Test
    public void delete_Should_Throw_When_WarehouseHasParcelAttached() {
        var warehouse = createMockWarehouse();
        var creator = createMockEmployee();
        var parcel = createMockParcel();
        parcel.setWarehouse(warehouse);

        List<Parcel> parcels = new ArrayList<>();
        parcels.add(parcel);

        Mockito.when(parcelRepository.filterParcels(Optional.empty(), Optional.empty(),Optional.empty(), Optional.of(warehouse.getId()), Optional.empty(), Optional.empty()))
                .thenReturn(parcels);
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> warehouseService.delete(warehouse.getId(),creator));
    }

    @Test
    public void delete_Should_CallRepository_When_CustomerIsCreatorAndThereAreNoParcelsAttached() {
        var warehouse = createMockWarehouse();
        var creator = createMockEmployee();
        Mockito.when(parcelRepository.filterParcels(Optional.empty(), Optional.empty(),Optional.empty(), Optional.of(warehouse.getId()), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        warehouseService.delete(warehouse.getId(),creator);

        Mockito.verify(warehouseRepository, Mockito.times(1))
                .delete(warehouse.getId());
    }

    @Test
    public void getAll_Should_CallRepository() {
        warehouseService.getAll();
        Mockito.verify(warehouseRepository, Mockito.times(1)).getAll();
    }

}
