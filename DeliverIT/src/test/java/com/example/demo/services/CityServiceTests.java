package com.example.demo.services;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.*;
import com.example.demo.repositories.contracts.CityRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.demo.Helpers.createMockCountry;

@ExtendWith(MockitoExtension.class)
public class CityServiceTests {

    @Mock
    CityRepository cityRepository;

    @InjectMocks
    CityServiceImpl cityService;

    @Test
    public void getAll_Should_CallRepository() {
        cityService.getAll();
        Mockito.verify(cityRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnCity_When_MatchExists() {

        //Arrange
        Mockito.when(cityRepository.getById(2))
                .thenReturn(new City(2, "Sofia", createMockCountry()));

        //Act
        City result = cityService.getById(2);

        //Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("Sofia",  result.getName());
        Assertions.assertEquals(createMockCountry().getId(), result.getCountry().getId());

    }

    @Test
    public void getById_Should_ThrowException_When_CountryDoesNotExist(){
        //Arrange & Act
        Mockito.when(cityRepository.getById(Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> cityService.getById(23));
    }

}
