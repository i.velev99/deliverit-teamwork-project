package com.example.demo.services;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Country;
import com.example.demo.repositories.contracts.CountryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.demo.Helpers.createMockCountry;

@ExtendWith(MockitoExtension.class)
public class CountryServiceTests {

    @Mock
    CountryRepository repository;

    @InjectMocks
    CountryServiceImpl service;

    @Test
    public void getById_Should_ReturnCountry_When_MatchExists(){
        //Arrange
        var mockCountry = createMockCountry();
        Mockito.when(repository.getById(mockCountry.getId()))
                .thenReturn(mockCountry);

        //Act
        Country result = service.getById(1);

        //Assert
        Assertions.assertEquals(1,result.getId());
        Assertions.assertEquals("Bulgaria",result.getName());
    }

    @Test
    public void getById_Should_ThrowException_When_CountryDoesNotExist(){
        //Arrange & Act
        Mockito.when(repository.getById(Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getById(23));
    }

    @Test
    public void getByName_Should_ReturnCountry_When_MatchExist(){
        //Arrange
        var mockCountry = createMockCountry();
        Mockito.when(repository.getByName(mockCountry.getName()))
                .thenReturn(mockCountry);

        //Act
        Country result = service.getByName("Bulgaria");

        //Assert
        Assertions.assertEquals(1,result.getId());
        Assertions.assertEquals("Bulgaria",result.getName());
    }

    @Test
    public void getByName_Should_ThrowException_When_CountryDoesNotExist(){
        //Arrange & Act
        Mockito.when(repository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getByName("NotCountry"));
    }

    @Test
    public void getAll_Should_CallRepository() {
        service.getAll();
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

}
