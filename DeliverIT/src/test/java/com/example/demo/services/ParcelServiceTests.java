package com.example.demo.services;

import com.example.demo.models.Parcel;
import com.example.demo.models.Shipment;
import com.example.demo.models.Status;
import com.example.demo.repositories.contracts.*;
import com.example.demo.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.demo.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ParcelServiceTests {

    @Mock
    private ParcelRepository repository;
    @Mock
    private ShipmentRepository shipmentRepository;
    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private WarehouseRepository warehouseRepository;

    @InjectMocks
    private ParcelServiceImpl parcelService;


    @Test
    public void create_Should_ThrowException_When_WhenShipmentDifferentFromPreparing() {
        //Arrange
        var mockParcel = createMockParcel();
        var mockShipment = createMockShipment();
        var mockUser = createMockCustomer();
        mockParcel.setCustomer(mockUser);
        mockShipment.setStatus(new Status(2, "On the way"));
        mockParcel.setShipment(mockShipment);
        var mockEmployee = createMockEmployee();

//
//        Mockito.when(Utils.checkEmployee(mockEmployee, ""))
//                .then(mockEmployee.getRole().getId())
//                .thenReturn(2);

        Mockito.when(shipmentRepository.getById(mockParcel.getShipment().getId()))
                .thenReturn(mockShipment);

        //Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> parcelService.create(mockParcel,mockEmployee));
    }

    @Test
    public void create_Should_CallInRepository() {
        //Arrange
        var mockParcel = createMockParcel();
        var mockShipment = createMockShipment();
        mockParcel.setShipment(mockShipment);
        var mockUser = createMockCustomer();
        mockParcel.setCustomer(mockUser);
        var mockEmployee = createMockEmployee();
        //Act
        Mockito.when(shipmentRepository.getById(mockParcel.getShipment().getId()))
                .thenReturn(mockShipment);

        parcelService.create(mockParcel,mockEmployee);

        //Assert
        Mockito.verify(repository, Mockito.times(1))
                .create(Mockito.any(Parcel.class));
    }

    @Test
    public void update_Should_ThrowException_When_WhenShipmentIsOnTheWay() {
        //Arrange
        var mockParcel = createMockParcel();
        var mockShipment = createMockShipment();
        mockShipment.setStatus(new Status(2, "On the way"));
        mockParcel.setShipment(mockShipment);
        var mockUser = createMockCustomer();
        mockParcel.setCustomer(mockUser);
        var mockEmployee = createMockEmployee();

        //Act
        Mockito.when(parcelService.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        //Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> parcelService.update(mockParcel,mockEmployee));
    }

    @Test
    public void update_Should_CallInRepository() {
        //Arrange
        var mockParcel = createMockParcel();
        var mockShipment = createMockShipment();
        mockParcel.setShipment(mockShipment);
        var mockUser = createMockCustomer();
        mockParcel.setCustomer(mockUser);
        var mockEmployee = createMockEmployee();

        //Act
        Mockito.when(parcelService.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        parcelService.update(mockParcel,mockEmployee);

        //Assert
        Mockito.verify(repository, Mockito.times(1))
                .update(Mockito.any(Parcel.class));
    }

    @Test
    public void delete_Should_ThrowException_When_WhenShipmentIsOnTheWay() {
        //Arrange
        var mockParcel = createMockParcel();
        var mockShipment = createMockShipment();
        mockShipment.setStatus(new Status(2, "On the way"));
        mockParcel.setShipment(mockShipment);
        var mockUser = createMockCustomer();
        mockParcel.setCustomer(mockUser);
        var mockEmployee = createMockEmployee();
        //Act
        Mockito.when(parcelService.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        //Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> parcelService.delete(mockParcel.getId(), mockEmployee));
    }

    @Test
    public void delete_Should_CallRepository() {
        //Arrange
        var mockParcel = createMockParcel();
        var mockShipment = createMockShipment();
        mockParcel.setShipment(mockShipment);
        var mockUser = createMockCustomer();
        mockParcel.setCustomer(mockUser);
        var mockEmployee = createMockEmployee();

        //Act
        Mockito.when(parcelService.getById(mockParcel.getId()))
                .thenReturn(mockParcel);
        parcelService.delete(mockShipment.getId(),mockEmployee);

        //Assert
        Mockito.verify(repository, Mockito.times(1))
                .delete(1);
    }
}
