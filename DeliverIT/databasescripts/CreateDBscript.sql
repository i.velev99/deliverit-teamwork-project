create table categories
(
    category_id   int auto_increment
        primary key,
    category_name varchar(45) null,
    constraint categories_category_name_uindex
        unique (category_name)
);

create table countries
(
    country_id int auto_increment
        primary key,
    name       text null,
    constraint Countries_name_uindex
        unique (name) using hash
);

create table cities
(
    city_id    int auto_increment
        primary key,
    city_name  text null,
    country_id int null,
    constraint cities_countries_fk
        foreign key (country_id) references countries (country_id)
);

create table addresses
(
    address_id int auto_increment
        primary key,
    address    text null,
    city_id    int null,
    constraint addresses_cities_fk
        foreign key (city_id) references cities (city_id)
);

create table roles
(
    role_id int auto_increment
        primary key,
    name    varchar(50) not null
);

create table statuses
(
    status_id   int auto_increment
        primary key,
    status_type varchar(45) null,
    constraint statuses_status_type_uindex
        unique (status_type)
);

create table shipments
(
    shipment_id    int auto_increment
        primary key,
    departure_date date not null,
    arrival_date   date not null,
    status_id      int  not null,
    constraint shipments_statuses_fk
        foreign key (status_id) references statuses (status_id)
);

create table users
(
    id         int auto_increment
        primary key,
    first_name varchar(100) not null,
    last_name  varchar(100) not null,
    email      varchar(200) not null,
    username   varchar(50)  not null,
    password   varchar(45)  not null
);

create table users_addresses
(
    user_id    int not null,
    address_id int null,
    constraint users_addresses_addresses_address_id_fk
        foreign key (address_id) references addresses (address_id),
    constraint users_addresses_users_id_fk
        foreign key (user_id) references users (id)
);

create table users_roles
(
    user_id int not null,
    role_id int not null,
    constraint users_roles_roles_role_id_fk
        foreign key (role_id) references roles (role_id),
    constraint users_roles_users_id_fk
        foreign key (user_id) references users (id)
);

create table warehouses
(
    warehouse_id int auto_increment
        primary key,
    address_id   int null,
    constraint warehouses_addresses_fk
        foreign key (address_id) references addresses (address_id)
);

create table parcels
(
    parcel_id    int auto_increment
        primary key,
    customer_id  int    not null,
    warehouse_id int null,
    weight       double not null,
    category_id  int null,
    shipment_id  int null,
    constraint parcels_categories_fk
        foreign key (category_id) references categories (category_id),
    constraint parcels_shipments_fk
        foreign key (shipment_id) references shipments (shipment_id),
    constraint parcels_users_id_fk
        foreign key (customer_id) references users (id),
    constraint parcels_warehouses_fk
        foreign key (warehouse_id) references warehouses (warehouse_id)
);


