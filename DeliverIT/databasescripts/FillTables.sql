INSERT INTO `countries` (`name`)
VALUES ('Bulgaria'),
       ('Germany'),
       ('USA'),
       ('Australia'),
       ('Russia'),
       ('China');

INSERT INTO `cities` (`city_name`, country_id)
VALUES ('Sofia', 1),
       ('Berlin', 2),
       ('Miami', 3),
       ('Sydney', 4),
       ('Moscow', 5),
       ('Beijing', 6),
       ('Samara', 5),
       ('Hong Kong', 6),
       ('Kresna', 1),
       ('Pirdop', 1),
       ('Hamburg', 2),
       ('Los Angeles', 3),
       ('Geelong', 4);

INSERT INTO `addresses` (`address`, city_id)
VALUES ('ul. Vasil Levski 3', 1),
       ('CH2934', 2),
       ('MF6655', 3),
       ('DF9876', 4),
       ('Vladimir Putin 10', 5),
       ('JG9563', 6),
       ('Olga 3', 7),
       ('KR6969', 8),
       ('ul. Struma 21', 9),
       ('ul. Metodi Stoqnov 28', 10),
       ('HB6523', 11),
       ('LA3696', 12),
       ('Fagrot 4', 13);

INSERT INTO `categories` (`category_name`)
VALUES ('Medical'),
       ('Electrical'),
       ('Clothes'),
       ('Cosmetics'),
       ('Home Furnitures'),
       ('Sports');


INSERT INTO `warehouses` (`address_id`)
VALUES (1),
       (2),
       (3),
       (4),
       (5),
       (6),
       (7),
       (8),
       (9),
       (10),
       (11),
       (12),
       (13);

INSERT INTO `statuses` (`status_type`)
VALUES ('Preparing'),
       ('On the way'),
       ('Completed');

INSERT INTO `shipments` (departure_date, `arrival_date`, status_id)
VALUES ('2021-3-26', '2021-4-22', 1),
       ('2021-4-1', '2022-5-3', 1),
       ('2021-5-3', '2022-7-22', 1),
       ('2021-5-6', '2022-8-10', 1),
       ('2021-2-25', '2021-3-29', 2),
       ('2021-2-10', '2021-4-20', 2),
       ('2021-3-14', '2021-5-20', 2),
       ('2021-3-19', '2021-6-6', 3),
       ('2020-12-10', '2021-1-10', 3),
    ('2021-1-9', '2021-2-15', 3),
    ('2021-1-15', '2021-2-23', 3);

INSERT INTO `users` (first_name, last_name, email, username, password)
VALUES ('Ivan', 'Velev', 'ivan.v@gmail.com', 'ivan', 'asdf'),
       ('Dimitar', 'Iliev', 'diliev15@gmail.com', 'mitaka', 'asdf'),
       ('John', 'Atanasov', 'jh@gmail.com', 'john', 'asdf'),
       ('Maria', 'Ilieva', 'maria.i@abv.bg', 'maria', 'asdf'),
       ('Vasil', 'Yanakov', 'vYanakov@gmail.bg', 'vasko', 'asdf'),
       ('Aleksandar', 'Makedonski', 'alex_s54@gmail.com', 'alex', 'asdf'),
       ('Ivan', 'Donev', 'ivan_d21@abv.bg', 'ivan1', 'asdf'),
       ('Mitko', 'Mutata', 'm.mutata@abv.bg', 'mutata', 'asdf'),
       ('Georgi', 'Petrov', 'goshi.p21@mail.bg', 'gogip', 'asdf'),
       ('Petar', 'Tomev', 'p.tomev@mail.ru', 'pepkata', 'asdf'),
       ('Bojidar', 'Mihov', 'bojkata123@yahoo.com', 'bojo', 'asdf'),
       ('Anton', 'Georgiev', 'anton.g21@mail.bg', 'tonkata', 'asdf');

INSERT INTO users_addresses(user_id, address_id)
VALUES (7, 1),
       (8, 2),
       (9, 3),
       (10, 4),
       (11, 5),
       (12, 6);

insert into roles(name)
VALUES ('Employee'),
       ('Customer');

INSERT INTO users_roles(user_id, role_id)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1),
       (7, 2),
       (8, 2),
       (9, 2),
       (10, 2),
       (11, 2),
       (12, 2);

INSERT INTO `parcels` (`customer_id`, warehouse_id, weight, category_id, shipment_id)
VALUES (7, 1, 40, 1, 1),
       (8, 1, 69, 2, 1),
       (9, 1, 2, 3, 2),
       (10, 2, 15, 2, 2),
       (8, 2, 20, 4, 3),
       (11, 3, 100, 4, 3),
       (8, 3, 300, 6, 4),
       (7, 4, 300, 6, 4),
       (9, 1, 55, 1, 5),
       (9, 2, 52, 2, 5),
       (11, 3, 25, 3, 6),
       (10, 4, 35, 4, 6),
       (8, 6, 4, 5, 7),
       (7, 7, 3, 6, 7),
       (8, 8, 12, 4, 8),
       (9, 9, 120, 3, 8),
       (11, 10, 12, 2, 9),
       (12, 11, 11, 1, 9),
       (12, 11, 39, 3, 10),
       (12, 12, 12, 5, 10);

